<?php
use backend\models\MembershipType;

$membershipModel = MembershipType::findOne($model->mem_type);
?>

<div class="row">
	<div class="col-xs-12">
		<img src="/images/header.png" style="width: 100%">
	</div>
</div>
<div style="padding: 20px 60px">
	<div class="row">

		<div>
			<span style="font-size: 16px; color: #3c3131; font-weight: bold;"> Badge Number : <?=$model->badge_number?></span>
			<img  style="padding-left: 320px" src="<?=$model->qrcode?>">
		</div>

        <div class="col-xs-12 table-responsive">

          <table class="table table-striped">
            
		<tbody>
            
            <tr>
              <th> Badge Number </th>
              <td> <?=$model->badge_number?> </td>
              <th> Badge Type </th>
              <td> <?=$membershipModel->type?> </td>
            </tr>
            <tr>
              <th> First Name </th>
              <td>  <?=$model->first_name?> </td>
              <th> Last Name  </th>
              <td>  <?=$model->last_name?> </td>
            </tr>
            <tr>
              <th> Gender </th>
              <td>  <?php if($model->gender=='0') echo'Male'; else {echo'Female';}?> </td>
              <th> YOB  </th>
              <td> <?=$model->yob?> </td>
            </tr>
            <tr>
              <th> City </th>
              <td> <?=$model->city?> </td>
              <th> State  </th>
              <td> <?=$model->state?> </td>
            </tr>
            <tr>
              <th> Phone </th>
              <td> <?=$model->phone?> </td>
              <th> Email  </th>
              <td> <?=$model->email?> </td>
            </tr>

             <tr>
              <th> Club Name </th>
              <td>  <?=$model->club_name?> </td>
              <th>   </th>
              <td>  </td>
            </tr>

            <tr>
              <th> Valid From </th>
              <td> <?=date('M d, Y',strtotime($model->incep))?> </td>
           		<th> Valid To </th>
              	<td> <?=date('M d, Y',strtotime($model->expires)) ?>  </td>
            	</tr>

           

            
            

            
            
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
</div>
           

