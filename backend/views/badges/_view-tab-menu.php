<?php 

use yii\helpers\url;
use yii\helpers\Html;
$urlStatus = yii::$app->controller->getCurrentUrl();

?>

<div class="btn-group pull-right">
                <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Renew / Update Badge', ['update', 'id' => $model->badge_number], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(' Delete', ['delete', 'id' => $model->badge_number], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

<ul class="nav nav-tabs">
    <li class="<?php if($urlStatus['actionId']=='view')echo'active';?>"> <a href="<?=Url::to(['/badges/view','id'=>$model->badge_number])?>"> Badge Holder Details </a></li>
    <li class="<?php if($urlStatus['actionId']=='view-subscriptions')echo'active';?>"> <a href="<?=Url::to(['/badges/view-subscriptions','id'=>$model->badge_number])?>"> Subscriptions Details</a></li>
    <li class="<?php if($urlStatus['actionId']=='view-renewal-history')echo'active';?>"><a href="<?=Url::to(['/badges/view-renewal-history','membership_id'=>$model->badge_number])?>"> Renewal History </a></li>
    <li class="<?php if($urlStatus['actionId']=='view-certifications-list')echo'active';?>"><a href="<?=Url::to(['/badges/view-certifications-list','id'=>$model->badge_number])?>"> Certifications </a></li>
    <li class="<?php if($urlStatus['actionId']=='view-work-credits')echo'active';?>"><a href="<?=Url::to(['/badges/view-work-credits','id'=>$model->badge_number])?>"> Work Credit Details</a></li>
    <li class="<?php if($urlStatus['actionId']=='view-work-credits-log')echo'active';?>"><a href="<?=Url::to(['/badges/view-work-credits-log','membership_id'=>$model->badge_number])?>"> View Credit Logs </a></li>
</ul>