<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Badges */

$this->title = 'Update Badges: ' . $model->badge_number;
$this->params['breadcrumbs'][] = ['label' => 'Badges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->badge_number, 'url' => ['view', 'id' => $model->badge_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="badges-update" ng-controller="UpdateBadgeController">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_update', [
        'model' => $model,
        'badgeSubscriptions'=>$badgeSubscriptions,
        'badgeCertification'=> $badgeCertification,
    ]) ?>

</div>
