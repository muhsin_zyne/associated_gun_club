<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\Badges */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="badges-form">

<div class="row">
   
    <div class="col-xs-12 col-sm-8">
        <?php $form = ActiveForm::begin(['id'=>'badgeUpdate']); ?>
        

        <div class="row">
            <div class="col-xs-6">
                <div class="profile-details">
                    <div class="info"> <span class="heading "> Name </span> <span class="pull-right"> <?= $model->prefix.'. '.$model->first_name.' '.$model->last_name.' '.$model->suffix?>  </span> </div>
                    <div class="info"> <span class="heading"> Gender </span> <span class="pull-right"> <?php if($model->gender==0) echo"Male"; else if($model->gender==1) echo "Female"; ?> </span> </div>
                   <!--   <div class="info"> <span class="heading"> Last Update </span> <span class="pull-right"><?= date('M d, Y h:i:s A',strtotime($model->updated_at))?> </span> </div>
                <div class="clearfix"> </div> -->
                </div>
            </div>
            
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'address')->textarea(['rows' => '3']) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'address_op')->textarea(['rows' => '3']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'city')->textInput([]) ?>
            </div>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'state')->textInput([]) ?>
            </div>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'zip')->textInput([]) ?>
            </div>
            <!-- <div class="col-xs-12 col-sm-2">
                <?=  $form->field($model, 'gender')->radioList([ '0'=>'Male', '1'=> 'Female']) ?>
            </div> -->
            <div class="col-xs-12 col-sm-3">
                 <?= $form->field($model, 'yob')->textInput(['maxlength'=>true]) ?>
            </div>
        </div>
        <div class="row">
            
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'email')->textInput([]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'email_op')->textInput([]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'phone')->textInput(['maxlength'=>true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'phone_op')->textInput([]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'ice_contact')->textInput([]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'ice_phone')->textInput(['maxlength'=>true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <?= $form->field($model, 'club_name')->dropDownList($model->getClubNamesList(), ['prompt'=>'select','id'=>'club-id','value'=>$model->club_id]); ?>
            </div>
            <div class="col-xs-12 col-sm-4">

                <?= $form->field($model, 'club_id')->textInput(['readonly' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'mem_type')->dropDownList($model->getMemberShipList(),['prompt'=>'select']) ?>
            </div>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'badge_type')->textInput(['readonly' => true ]) ?>
            </div>
            
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'incep')->textInput(['readonly' => true,'value'=>date('M d, Y h:i A',strtotime($model->incep))]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3" ng-if="membershipType=='2'">

           
            </div>
        </div>

            <?= $form->field($model, 'primary')->widget(DepDrop::classname(), [
                'type'=>DepDrop::TYPE_SELECT2,
                'options'=>['id'=>'primary-id'],
                'pluginOptions'=>[
                    'depends'=>['club-id'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/badges/get-family-badges'])
                ]
            ]);
            ?>



        <div class="row">

            <?php 
                $month =date('m');
                if($month<=10) {
                    $date = date('30-01-Y', strtotime('+1 year'));
                }
                else {
                    $date = date('30-01-Y', strtotime('+2 year'));
                }

             ?>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'expires')->textInput(['readonly' => true,'value'=> date('M d, Y',strtotime($model->expires)) ]) ?>
            </div>
             <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'wt_date')->textInput(['value'=>date('M d, Y',strtotime($model->wt_date)) ]) ?>
            </div>
             <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'wt_instru')->textInput([]) ?>
            </div>
        </div>
        <div class="row">
                <div class="col-xs-12">
                    <?php 
                        $remakrs_logs = json_decode($model->remarks,true);
                        rsort($remakrs_logs);
                    ?>

                    
                </div>
                <div class="col-xs-12 col-sm-12">
                <?= $form->field($model, 'remarks_temp')->textarea(['rows' => '2'])->label('Remarks') ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : '<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary pull-right']) ?>
                </div> 
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <h3> Remarks history </h3>
            </div>
            <div class="col-xs-12">
                <?=$this->render('_remarks',['remakrs_logs'=>$remakrs_logs])?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>  
    </div>

<?php 
$expireDate = date('Y-m-d', strtotime("+1 years",strtotime($model->expires)));
?>
   
   <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="box">
                <?php $form1 = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-6',
                            'wrapper' => 'col-sm-6',
                        ],
                    ],
                    'id' => 'form-order-article', 'enableClientValidation' => true, 'enableAjaxValidation' => false,
                    'action' => ['badges/renew-membership','membership_id'=>$model->badge_number],
                    'options' =>['enctype' => 'multipart/form-data']
                ]); ?>
                <h3 class="text-center"> BADGE RENEWAL </h3>
                <div style="margin-top: -25px">
                <?= $form1->field($badgeSubscriptions, 'total_credit')->hiddenInput(['value'=>$model->work_credits])->label(false) ?>
                <?= $form1->field($badgeSubscriptions, 'badge_number')->hiddenInput(['value'=>$model->badge_number,])->label(false)?>
                <?= $form1->field($badgeSubscriptions, 'expires')->textInput(['readOnly'=>true,'value'=>date('M d, Y',strtotime($expireDate))]) ?>
                <?= $form1->field($badgeSubscriptions, 'badge_fee')->textInput(['readOnly'=>true]) ?>
                <?= $form1->field($badgeSubscriptions, 'redeemable_credit')->textInput(['value'=>'','readOnly'=>true])->label('Credit') ?>
                <?= $form1->field($badgeSubscriptions, 'discount')->textInput([]) ?>
                <?= $form1->field($badgeSubscriptions, 'amount_due')->textInput(['readOnly'=>true]) ?>
                <?= $form1->field($badgeSubscriptions, 'sticker')->textInput([]) ?>
                
                
               
                <?= $form1->field($badgeSubscriptions, 'payment_type')->dropdownList(
                                                                        ['cash'=>'Cash','check'=>'Check','credit'=>'Credit Card','online'=>'Online','other'=>'Other'],['prompt'=>'Payment Type'])?>

                        <div class="form-group">
                            <?= Html::submitButton( '<i class="fa fa-refresh" aria-hidden="true"></i> RENEW BADGE', ['class' => 'btn btn-primary pull-right']) ?>
                        </div>
                </div>
                        <?php ActiveForm::end(); ?>
                <div class="clearfix"> </div>
            </div>
        </div>



        



        <div class="row">
            <div class="box">
                <?php $form2 = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-6',
                            'wrapper' => 'col-sm-6',
                        ],
                    ],
                   // 'id' => 'form-order-article', 'enableClientValidation' => true, 'enableAjaxValidation' => false,
                    'action' => ['badges/add-certification','membership_id'=>$model->badge_number],
                    'options' =>['enctype' => 'multipart/form-data']
                ]); ?>
                <h3 class="text-center"> CERTIFICATIONS </h3>
                <div>

                <?= $form2->field($badgeCertification, 'proc_date')->textInput(['value'=>date('M d, Y',strtotime(yii::$app->controller->getNowTime()))])->label('Date') ?>
                <?= $form2->field($badgeCertification, 'sticker')->textInput(['maxlength'=>true]) ?>
                <?= $form2->field($badgeCertification, 'certification_type')->dropdownList($badgeCertification->getcertificationList(),['prompt'=>'certification type']) ?>
                <?= $form2->field($badgeCertification, 'status')->dropdownList(['0'=>'Active','1'=>'Suspended','2'=>'Revoked'],['prompt'=>'status']) ?>
                
                <div class="form-group">
                    <?= Html::submitButton( '<i class="fa fa-plus-square" aria-hidden="true"></i> ADD CERTIFICATION', ['class' => 'btn btn-primary pull-right']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="clearfix"> </div>
            </div>
        </div>

   </div>
    
    
    
</div>  

</div>

<style> 
.wrap {
    min-height: 100%;
    height: auto;
    margin: 0 auto -60px;
    padding: 0 0 0px !important;
}
</style>
