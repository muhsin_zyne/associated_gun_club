<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Badges */

$this->title = $model->badge_number;
$this->params['breadcrumbs'][] = ['label' => 'Badges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$urlStatus = yii::$app->controller->getCurrentUrl();
?>
<div class="badges-view">
    <div class="row" > 
        <div class="col-xs-12">
            

    <?= $this->render('_view-tab-menu',['model'=>$model]) ?>

    
            <h3>Badge Holder Details </h3>
            <div class="col-xs-12 col-sm-4 pull-right">
                 <div class="qr-block pull-right">
                    <img src="<?=$model->qrcode?>">

                </div>
            </div>

            <div class="col-xs-12 col-sm-8">
                <div class="block-badge-view">

                   <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'badge_number',
                        'prefix',
                        'first_name',
                        'last_name',
                        'suffix',
                        'address:ntext',
                        'address_op:ntext',
                        'city',
                        'state',
                        'zip',
                        [
                            'attribute'=>'gender',
                            'value'=> function($model, $attribute) {
                                if($model->gender==0) return 'Male'; else return 'Female';
                            },
                        ],
                        'yob',
                       
                        'email:email',
                        'email_op:email',
                        'phone',
                        'phone_op',
                        'ice_contact',
                        'ice_phone',
                        'club_name',
                        'club_id',
                        'membershipType.type',

                        //'primary',
                        [
                            'attribute'=>'incep',
                            'value'=>function($model,$attribute) {
                                return date('M d, Y h:i A',strtotime($model->incep));
                            },
                        ],
                        /*[
                            'attribute'=>'expires',
                            'value'=>function($model,$attribute) {
                                return date('M d, Y',strtotime($model->expires));
                            },
                        ],
                        [
                            'attribute'=>'wt_date',
                            'value'=>function($model,$attribute) {
                                return date('M d, Y',strtotime($model->wt_date));
                            },
                        ],
                        //'qrcode:ntext',
                        'wt_instru',
                        'remarks:ntext',
                        [
                            'attribute'=>'badge_fee',
                            'value'=> function($model, $attribute) {
                                return money_format('$ %i', $model->badge_fee);
                            },
                        ],
                        [
                            'attribute'=>'discounts',
                            'value'=> function($model, $attribute) {
                                return money_format('$ %i', $model->discounts);
                            },
                        ],
                        [
                            'attribute'=>'amount_paid',
                            'value'=> function($model, $attribute) {
                                return money_format('$ %i', $model->amt_due);
                            },
                        ],
                        'payment_method',
                        [
                            'attribute'=>'status',
                            'value'=>function($model,$attribute) {
                                return ucfirst($model->status);
                            },
                        ],
                        'sticker',*/

                    ],
                ]) ?>
                   
                </div>
            </div>
        </div>













    </div>
</div>
        