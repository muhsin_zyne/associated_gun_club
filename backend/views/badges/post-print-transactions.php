<?php
use backend\models\Badges;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\export\ExportMenu;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ClubsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Post Print Transactions - '.date('M d, Y',strtotime(yii::$app->controller->getNowTime()));
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clubs-index">
    <div class="row">

        <div class="col-xs-12">
            <h2><?= Html::encode($this->title) ?></h2>
           
        </div>
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
             <?php $form = ActiveForm::begin([
                'id'=>'postPrintTransactionForm',
                'action' => ['/badges/post-print-transactions'],
                'method' => 'get',
            ]); ?>


           
            <!-- <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div> -->

            <?php echo $form->field($searchModel, 'created_at')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Transaction Date'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'mm/dd/yyyy'
                ]
            ])->label('Transaction Date') ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="row">
      
        <div class="col-xs-12">
            <?php 

                $gridColumns = [
                    //['class' => 'kartik\grid\SerialColumn'],
                    [
                        'header'=>'Badge Number',
                        'value' => 'badge_number',
                        'contentOptions' => ['class' => 'text-left'],
                    ],
                    [
                        'header'=>'Transaction Type',
                        'value' => 'transaction_type',
                    ],
                    [
                        'header'=> 'Name',
                        'value' => function($model) {
                            $badgeArry = Badges::findOne($model->badge_number);
                            return $badgeArry->prefix.' '.$badgeArry->first_name.' '.$badgeArry->last_name.' '.$badgeArry->suffix;
                        },
                    ],
                    [
                        'header'=>'Club',
                        'value'=>'clubDetails.club_name',
                    ],
                    [
                        'header'=>'Fee',
                        'value'=>function($model) {
                             return money_format('$%i', $model->badge_fee);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'header'=>'Paid Amount',
                        'value'=>function($model) {
                             return money_format('$%i', $model->paid_amount);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    
                   
                   
                    
                   
                ];

                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'batchSize' => 10,
                    'filename'=>  $this->title,
                    'target' => '_blank',
                    'folder' => '@webroot/export', // this is default save folder on server
                ]) . "<hr>\n".
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                ]);
                     
            ?>
        </div>
       
    </div>
</div>
