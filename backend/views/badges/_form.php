<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\Badges */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="badges-form">
<?php $form = ActiveForm::begin(); ?>
<div class="row">

    <div class="col-xs-12 col-sm-8">
        <div class="row">
            <?php if(!$model->isNewRecord)  { ?>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'badge_number')->textInput(['readOnly'=>true]) ?>  
            </div>
            <?php } ?>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-2">
                <?= $form->field($model, 'prefix')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-4">
                <?= $form->field($model, 'first_name')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-4">
                <?= $form->field($model, 'last_name')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-2">
                <?= $form->field($model, 'suffix')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'address')->textarea(['rows' => '3']) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'address_op')->textarea(['rows' => '3']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'city')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-2">
                <?= $form->field($model, 'state')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-2">
                <?= $form->field($model, 'zip')->textInput([]) ?>
            </div>
            <div class="col-xs-12 col-sm-2">
                <?=  $form->field($model, 'gender')->radioList([ '0'=>'Male', '1'=> 'Female']) ?>
                
            </div>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'yob')->textInput(['maxlength'=>true]) ?>
                
            </div>
        </div>
        <div class="row">
            
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'email')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'email_op')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'phone')->textInput(['maxlength'=>true,'readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'phone_op')->textInput(['maxlength'=>true,'readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'ice_contact')->textInput(['readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'ice_phone')->textInput(['maxlength'=>true,'readonly'=> $model->isNewRecord ? false : true,]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <?php if($model->isNewRecord) { ?>
                <?= $form->field($model, 'club_name')->dropDownList($model->getClubNamesList(), ['prompt'=>'select','id'=>'club-id']); ?>
                <?php } else { ?>
                <?= $form->field($model, 'club_name')->textInput(['readonly' => true]) ?>
                <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-4">

                <?= $form->field($model, 'club_id')->textInput(['readonly' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'mem_type')->dropDownList($model->getMemberShipList(),['prompt'=>'select']) ?>
            </div>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'badge_type')->textInput(['readonly' => true ])->label('Membership ID') ?>
            </div>
            
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'incep')->textInput(['readonly' => true,'value'=>date('M d, Y h:i A')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3" ng-if="membershipType=='2'">

           
            </div>
        </div>

            <?= $form->field($model, 'primary')->widget(DepDrop::classname(), [
                'type'=>DepDrop::TYPE_SELECT2,
                'options'=>['id'=>'primary-id'],
                'pluginOptions'=>[
                    'depends'=>['club-id'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/badges/get-family-badges'])
                ]
            ]);
            ?>



        <div class="row">

            <?php 
                $month =date('m');
                if($month<=10) {
                    $date = date('30-01-Y', strtotime('+1 year'));
                }
                else {
                    $date = date('30-01-Y', strtotime('+2 year'));
                }

             ?>
            <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'expires')->textInput(['readonly' => true,'value'=> date('M d, Y',strtotime($date)) ]) ?>
            </div>
             <div class="col-xs-12 col-sm-3">
                <?= $form->field($model, 'wt_date')->textInput(['value'=>date('M d, Y') ]) ?>
            </div>
             <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'wt_instru')->textInput([]) ?>
            </div>
        </div>
        <div class="row">
                <div class="col-xs-12 col-sm-12">
                <?= $form->field($model, 'remarks')->textarea(['rows' => '4']) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-save"> </i> SAVE' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?= Html::a('<i class="fa fa-eraser"> </i> Clear',['/badges/create'],['class' => 'btn btn-danger']) ?>
                </div> 
            </div>

        </div>
    
    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="summary-block-payment box">
                <div class="col-xs-12 col-sm-12">

                    <?= $form->field($model, 'badge_fee')->widget(MaskMoney::classname(), [

                        'pluginOptions' => [
                        'allowNegative' => false,
                        ]
                    ]); ?>
                   
                </div>
                <div class="col-xs-12 col-sm-12">

                <?= $form->field($model, 'discounts')->widget(MaskMoney::classname(), [
                        
                        'pluginOptions' => [
                        'allowNegative' => false,
                        ]
                    ]); ?>

                </div>
                <div class="col-xs-12 col-sm-12">
                    <?= $form->field($model, 'amt_due')->widget(MaskMoney::classname(), [
                        
                        'pluginOptions' => [
                        'allowNegative' => false,
                        ]
                    ]); ?>
                   
                </div>
                <div class="col-xs-12 col-sm-12">
                    <?= $form->field($model, 'payment_method')->dropDownList(['cash'=>'Cash','check'=>'Check','credit'=>'Credit Card','online'=>'Online','other'=>'Other'],['prompt'=>'select']) ?>
                </div>

                 <div class="col-xs-12 col-sm-12">
                    <?= $form->field($model, 'sticker')->textInput(['maxlength'=>true]) ?>
                </div>
                <div class="clearfix"></div>
            </div>
            

        </div>
    </div>
    
</div>  
<?php ActiveForm::end(); ?>
</div>

