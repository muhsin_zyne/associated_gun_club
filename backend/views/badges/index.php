<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Badges;
use kartik\widgets\DatePicker;
use kartik\daterange\DateRangePicker;

$badgesModel = new Badges();

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BadgesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View / Print / Update Badge Holder Profile';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="row">
     <div class="col-xs-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <?= $this->render('_search',['model'=>$searchModel])?>
    </div>
</div>
<div class="row">

    <div class="col-xs-12">
        <p> <?= Html::a('<i class="fa fa-plus-square" aria-hidden="true"></i> Create Badges', ['create'], ['class' => 'btn btn-success pull-right']) ?></p>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'badge_number',
            //'mem_type',
            [
                'header'=>'Badge Type',
                'attribute'=>'mem_type',
                'value' => function($model, $attribute){switch ($model->mem_type) {
                    case '1':
                        return 'Primary';
                        break;
                    case '2':
                        return 'Family';
                        break;
                    case '3':
                        return 'Junior';
                        break;
                    case '4':
                        return 'Life';
                        break;
                    default:
                        # code...
                        break;
                }},
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'mem_type',$badgesModel->getMemberShipList(),['class'=>'form-control','prompt' => 'All']),
            ],
            
            'first_name',
            'last_name',
            [
                'header'=>'Expire Date',
                'value' => function($model, $attribute) {
                    return date('M d, Y',strtotime($model->expires));
                },



                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'expire_condition',['active'=>'Active','active+2'=>'Active +2','expired<2'=>'Expired <2','expired>2'=>'Expired >2','inactive'=>'Inactive','all'=>'All'],['value'=>$searchModel->expire_condition !=null ? $searchModel->expire_condition : 'active+2','class'=>'form-control']),
            ],
            //'expires',
            [
                'header'=>'Status',
                'value'=>function($model,$attribute) {
                    return ucfirst($model->status);
                },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status',['active'=>'Active','pending'=>'Pending','suspended'=>'Suspended'
                    ],['class'=>'form-control','prompt' => 'All']),
            ],
            

            //['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{print} {view}{update}{delete}',
                            'buttons'=>[
                                'print' => function($url,$model) {
                                    return  Html::a(' <span class="glyphicon glyphicon-print"></span> ', ['/badges/print-view','id'=>$model->badge_number], [
                                            'target'=>'_blank',
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'Print',
                                        ]); 

                                },

                                  'update' => function ($url, $model) {    
                                        return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', $url, [
                                                'data-toggle'=>'tooltip',
                                                'data-placement'=>'top',
                                                'title'=>'Edit',
                                                'class'=>'edit_item',
                                            ]); 
                                    },
                                    'view' => function($url,$model) {
                                    return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', $url, [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'View',
                                        ]); 

                                },

                                'delete' => function($url,$model) {
                                    return  Html::a(' <span class="glyphicon glyphicon-trash"></span> ', $url, [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'Delete',
                                            'data' => [
                                                'confirm' => 'Are you sure you want to delete this item?',
                                                'method' => 'post',
                                            ],
                                        ]); 

                                },

                                


                          ]                            
            ],
        ],
    ]); ?>
    </div>
</div>
<div class="badges-index">

    
</div>
