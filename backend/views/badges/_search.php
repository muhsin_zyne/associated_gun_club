<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\search\BadgesSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-xs-12">
        <h3> Filters <h3>
    </div>
</div>

<div class="row">

    <div class="badges-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-sm-6">
<!-- 
<?=  $form->field($model, 'expire_date_range', [
    'options'=>['class'=>'drp-container form-group']
    ])->widget(DateRangePicker::classname(), [
    'presetDropdown'=>true,
    'hideInput'=>true,
    'pluginOptions' => [
            'locale'=>[
            'format'=>'MM/DD/YYYY',
        ],
        'opens'=>'left'
        ],
    ]); ?>
  -->

    <?= $form->field($model, 'expire_condition')->dropDownlist(['active'=>'Active','active+2'=>'Active +2','expired<2'=>'Expired <2','expired>2'=>'Expired >2','inactive'=>'Inactive','all'=>'All'],['value'=>$model->expire_condition !=null ? $model->expire_condition : 'active+2'])->label('Expire Type') ?>

    </div>
    <div class="col-sm-6">
        <div class=" form-group btn-group pull-right">
            <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i> Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('<i class="fa fa-eraser" aria-hidden="true"></i> Reset', ['/badges/index'],['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>
</div>

