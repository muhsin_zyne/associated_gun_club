<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BadgePayments */

$this->title = 'Update Badge Payments: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Badge Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="badge-payments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
