<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BadgePayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="badge-payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'badge_number')->textInput() ?>

    <?= $form->field($model, 'valid_from')->textInput() ?>

    <?= $form->field($model, 'valid_true')->textInput() ?>

    <?= $form->field($model, 'payment_type')->dropDownList([ 'cash' => 'Cash', 'check' => 'Check', 'credit' => 'Credit', 'online' => 'Online', 'other' => 'Other', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'pending' => 'Pending', 'expired' => 'Expired', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
