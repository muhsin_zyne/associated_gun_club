<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BadgePayments */

$this->title = 'Create Badge Payments';
$this->params['breadcrumbs'][] = ['label' => 'Badge Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="badge-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
