<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Add / Remove Authorized Users ';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= Html::encode($this->title) ?></h2>
            <p> <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success pull-right']) ?> </p>
            <?php Pjax::begin(); ?>    
            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        
                        'username',
                        'email:email',
                        'full_name',
                        'privilege' => [   'header'=>'Privilege',
                            'value' => function($model, $attribute){ if($model->privilege==1) {return 'Root';} else if($model->privilege==2) {return 'Admin';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'privilege', ['1' => 'Root', '2' => 'Admin'],['class'=>'form-control','prompt' => 'All']),
                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>

        </div>
    </div>

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



</div>
