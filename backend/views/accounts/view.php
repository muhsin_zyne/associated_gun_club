<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = ['label' => 'Add / Remove Authorized Users', 'url' => ['/accounts/index']];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'full_name',
            [
                'attribute' =>'privilege',
                'value' => function($model) { if($model->privilege==1) return'Root'; else if($model->privilege==2) return 'Admin'; },
            ],
            [
                'attribute' =>'status',
                'value' => function($model) { if($model->status==10) return'Active'; else return 'Inactive'; },
            ],
            [
                'attribute' =>'created_at',
                'value' => function($model) { return date('M d, Y H:i:s',$model->created_at); },
            ],
            [
                'attribute' =>'updated_at',
                'value' => function($model) { return date('M d, Y H:i:s',$model->updated_at); },
            ],
        ],
    ]) ?>

</div>
