<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create user';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = ['label' => 'Add / Remove Authorized Users', 'url' => ['/accounts/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row">
        <div class="col-xs-12">
             <h2><?= Html::encode($this->title) ?></h2>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'full_name')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'confirm_password')->passwordInput() ?>
                <?= $form->field($model, 'privilege')->dropDownList(['1'=>'Root','2'=>'Admin'],['prompt'=>'Privilege']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-success pull-right', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
