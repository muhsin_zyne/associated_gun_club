<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Clubs 
*/
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
	<div class="col-xs-12">

		<?php if(isset($errorArray)) {

		?>
		<div class="alert alert-warning">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		    <strong>Warning!</strong> Importing field for the following Clubs due to data duplication
		    
		    <table class="table table-striped" style="max-height: 300px">
		    <thead>
		      <tr>
		        <th>#</th>
		        <th>Club ID</th>
		        <th>Club Name</th>
		        <th>Short Name</th>
		      </tr>
		    </thead>
		    <tbody>

		    	<?php foreach ($errorArray as $key => $value) {
		    		$key = $key+1;
		    		echo '

		    		<tr>
		        		<td>'.$key.'</td>
		        		<td>'.$value['club_id'].'</td>
		        		<td>'.$value['club_name'].'</td>
		        		<td>'.$value['short_name'].'</td>
		      		</tr>

		    		';
		    		
		    	} ?>

		   </tbody>
		  </table>
		</div>
		<?php 	
		} ?>
	</div>
</div>

 <div class="row">
  <div class="col-sm-12">
   <form name='fileUpload' method="post" action="" enctype="multipart/form-data">
   	<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
   	<div class="form-group">
   		<label> Import Clubs</label>
   		<input type="file" name="file" class="form-control">
   	</div>
    <div class="form-group">
    	<input type="submit" value="Import " name="upload" class="btn btn-primary pull-right">
    </div>

    
   </form>

  </div>
 </div>

