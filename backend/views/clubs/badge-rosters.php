<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

use kartik\export\ExportMenu;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ClubsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Create Badge Rosters for Clubs';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clubs-index">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= Html::encode($this->title) ?></h2>
            <?php $form = ActiveForm::begin([
                'action' => ['/clubs/badge-rosters'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'club_id_dummy')->dropdownList($searchModel->getClubList(),['prompt'=>'club name'])->label('Club Name'); ?>

            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
    </div>
    <div class="row">
        <?php if($dataProvider->count>0) { ?>
        <div class="col-xs-12">
            <?php 

                $gridColumns = [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'header'=>'Badge Number',
                        'value' => 'badge_number',
                    ],
                    [
                        'header'=>'Sticker',
                        'value' => function($model) {
                            return $model->sticker;
                        },
                    ],
                    [
                        'header'=>'Full Name',
                        'value' => function($model) {
                            return $model->prefix.' '.$model->first_name.' '.$model->last_name.' '.$model->suffix;
                        },
                    ],


                    [
                        'header'=>'YOB',
                        'value' => function($model) {
                            return $model->yob;
                        },
                    ],
                    /*[
                        'header'=>'City',
                        'value' => function($model) {
                            return $model->city;
                        },
                    ],
                    [
                        'header'=>'State',
                        'value' => function($model) {
                            return $model->state;
                        },
                    ],
                    [
                        'header'=>'Zip',
                        'value' => function($model) {
                            return $model->zip;
                        },
                    ],*/
                    [
                        'header'=>'Email',
                        'value' => function($model) {
                            return $model->email;
                        },
                    ],
                    
                    [
                        'header'=>'Phone',
                        'value' => function($model) {
                            return $model->phone;
                        },
                    ],
                    [
                        'header'=>'Membership Type',
                        'value'=> 'membershipType.type',
                    ],
                    [
                        'header'=>'Expire Date',
                        'value'=> function($model) {
                            return date('M d, Y',strtotime($model->expires));                            
                        }
                    ]
                   
                   
                    
                   
                ];

                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'batchSize' => 10,
                    'filename'=>  $exportFileName,
                    'target' => '_blank',
                    'folder' => '@webroot/export', // this is default save folder on server
                ]) . "<hr>\n".
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                ]);
                     
            ?>
        </div>
        <?php } ?>
    </div>
</div>
