<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkCredits */
/* @var $form yii\widgets\ActiveForm */

$session = Yii::$app->session;
$stickyData = $session->get('stickyData');
if(!empty($stickyData)) {
    $model->work_date = $stickyData['work_date'];
    $model->authorized_by = $stickyData['authorized_by'];
    $model->work_hours = $stickyData['work_hours'];
    $model->project_name = $stickyData['project_name'];
    $model->remarks = $stickyData['remarks'];
}
?>

<div class="work-credits-form" ng-controller="BadgeCreditFrom">
    <?php $form = ActiveForm::begin([
            'id'=>'creditEntryForm',
        ]); ?>
    <div class="row">
        <div class="col-sm-4">
        <?php if($model->isNewRecord) {

            if($badgeArray!=null) {
                echo $form->field($model, 'badge_number')->textInput(['readOnly'=>'true','value'=>$badgeArray->badge_number]);
                echo $form->field($model, 'work_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Work Date'],

                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'M dd, yyyy',

                        ]
                ]);
            }
            else {

                echo $form->field($model, 'badge_number')->widget(Select2::classname(), [
                    'data' => $model->getBadgeNumbers(),
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a badge ...'],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],
                ]);

                echo $form->field($model, 'work_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Work Date'],

                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'M dd, yyyy',

                        ]
                ]);

            }

        
        }
        else {
        echo $form->field($model, 'badge_number')->textInput(['readOnly'=>'true']);
        echo $form->field($model, 'work_date')->textInput(['readOnly'=>'true','value'=>date('M d, Y',strtotime($model->work_date))]);
        }

        ?>

            
            
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'badge_holder_name')->textInput(['readOnly'=>'true']) ?>
            <?php if(!$model->isNewRecord) {  ?>
                <?= $form->field($model, 'work_hours_new')->textInput(['value'=>$model->work_hours]) ?>
                <?= $form->field($model, 'work_hours')->hiddenInput([])->label(false) ?>
            <?php }  else { ?>
                <?= $form->field($model, 'work_hours')->textInput() ?>
            <?php } ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'authorized_by')->textInput(['maxlength' => true])->label('Authorized By') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'project_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
        </div>  
    </div>
    
    
    

    <div class="btn-group pull-right">
        <?php if($model->isNewRecord){ ?>
        <?= Html::a('Next <i class="fa fa-arrow-right"> </i>',[''],['class' => 'btn btn-primary next-Credit']) ?>
        <?php } ?>
	<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
         <?php if($model->isNewRecord){ ?>
        <?= Html::a('Done <i class="fa fa-save"> </i>',[''],['class' => 'btn btn-danger done-Credit']) ?>
    	<?php } ?>
    </div>

    <?php ActiveForm::end(); ?>  

</div>
