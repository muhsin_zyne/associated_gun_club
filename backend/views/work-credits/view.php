<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkCredits */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = ['label' => 'Work Credits', 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-credits-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('View Badge Profile', ['/badges/view', 'id' => $model->badge_number], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'badge_number',
            [    
                'attribute'=>'work_date',
                'value'=>function($model,$attribute) {
                    return date('M d, Y',strtotime($model->work_date));
                },
            ],
            'work_hours',
            'project_name',
            'remarks:ntext',
            'authorized_by',
            [
                'attribute'=>'created_at',
                'value'=> function($model, $attribute) {
                    return date('M d, Y h:i A', strtotime($model->created_at));
                },
            ],
            [
                'attribute'=>'updated_at',
                'value'=> function($model, $attribute) {
                    return date('M d, Y h:i A', strtotime($model->updated_at));
                },
            ],
        ],
    ]) ?>

</div>
