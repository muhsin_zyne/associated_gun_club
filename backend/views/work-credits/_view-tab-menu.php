<?php 

use yii\helpers\url;
use yii\helpers\Html;
$urlStatus = yii::$app->controller->getCurrentUrl();
?>

<ul class="nav nav-tabs">
    <li class="<?php if($urlStatus['requestUrl']=='/work-credits/credit-transfer?type=init')echo'active';?>"> <a href="<?=Url::to(['/work-credits/credit-transfer','type'=>'init'])?>">Pending Requests</a></li>
    <li class="<?php if($urlStatus['requestUrl']=='/work-credits/credit-transfer?type=success')echo'active';?>"> <a href="<?=Url::to(['/work-credits/credit-transfer','type'=>'success'])?>">Success Requests</a></li>
    
</ul>

 <?php if($urlStatus['requestUrl']=='/work-credits/credit-transfer?type=init') $this->title = 'Pending Requests';
else if($urlStatus['requestUrl']=='/work-credits/credit-transfer?type=success') $this->title = 'Success Requests';

?>
<h1><?= Html::encode($this->title) ?></h1>