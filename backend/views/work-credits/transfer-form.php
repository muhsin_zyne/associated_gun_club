<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = 'Credit Transfer form';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = ['label' => 'Credit Transfer', 'url' => ['/work-credits/credit-transfer','type'=>'init']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="work-transfer-form" ng-controller="WorkTransferForm">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-xs-12">
		<h3> Transfer From </h3>
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'from_badge_number')->widget(Select2::classname(), [
                    'data' => $WorkCreditsModel->getBadgeNumbers(),
                    'options' => ['placeholder' => 'Select a badge ...'],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],
                ]); ?>
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'from_badge_name')->textInput(['readOnly'=>true]) ?>
		</div>

		<div class="col-xs-4">
			<div id="credit-block">
				<?= $form->field($model, 'total_credit_avl')->textInput(['readOnly'=>true]) ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
		<h3> Transfer To </h3>
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'to_badge_number')->widget(Select2::classname(), [
                    'data' => $WorkCreditsModel->getBadgeNumbers(),
                    'options' => ['placeholder' => 'Select a badge ...'],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],
                ]); ?>

		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'to_badge_name')->textInput([]) ?>

		</div>

		<div class="col-xs-4">
			<?= $form->field($model, 'work_hours')->textInput([]) ?>
			
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<?= $form->field($model, 'note')->textArea(['rows'=>1]) ?>
		</div>
		<div class="col-xs-4">
			<?= $form->field($model, 'approved_by')->textInput(['maxlength'=>true]) ?>

			<div class="form-group">
       			<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
    		</div>
		</div>
		
	</div>

	<?php ActiveForm::end(); ?>  
</div>