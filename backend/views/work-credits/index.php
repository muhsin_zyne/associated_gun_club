<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WorkCreditsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Work Credits';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-credits-index">
    <div class="row">
        <div class="col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>   
            <div class="btn-group pull-right"> <?= Html::a('Create Work Credit', ['create'], ['class' => 'btn btn-success ']) ?>
             <?= Html::a('Import', ['/work-credits/import'], ['class' => 'btn btn-primary pull-right pull-right']) ?> 
            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'badge_number',
                    [
                        'header'=>'Work Date',
                        'attribute'=>'work_date',
                        'value'=>function($model) {
                            return date('M d, Y',strtotime($model->work_date));
                        },
                    ],
                    'project_name',
                    [
                        'attribute'=>'work_hours',
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'header'=>'Auth',
                        'attribute'=>'authorized_by',
                        'value'=> 'authorized_by',
                    ],

                    'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status==1) {return 'BULK IMP';} else if($model->status==0) {return 'Inactive';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'BULK IMP', '1' => 'Inactive'],['class'=>'form-control','prompt' => 'All']),
                    ],
                    'remarks',
                    [
                        'header'=>'Procdate',
                        'attribute'=>'updated_at',
                        'value'=> function ($model) {
                                return date('M d, Y',strtotime(($model->updated_at)));
                        },
                    ],

                    [
                        'header'=>'Who',
                        'attribute'=>'created_role',
                        'value'=>function($model) {
                            if($model->created_role==1) {
                                return 'Admin';
                            }
                        },
                    ],

                    // 'remarks:ntext',
                    // 'authorized_by',
                    // 'status',
                    // 'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
    
</div>
