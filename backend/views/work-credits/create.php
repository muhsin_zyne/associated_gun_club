<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\WorkCredits */

$this->title = 'Create Work Credit';
$this->params['breadcrumbs'][] = ['label' => 'Admin Function', 'url' => ['/badge/admin-function']];
$this->params['breadcrumbs'][] = ['label' => 'Work Credits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="work-credits-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'badgeArray'=>$badgeArray,
    ]) ?>

</div>
