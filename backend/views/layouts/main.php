<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <link rel="stylesheet" href="http://projects.itekk.us/associatedgunclubs/backend/web/css/sweetalert.css">
    <link rel="stylesheet" href="http://projects.itekk.us/associatedgunclubs/backend/web/css/waitMe.css"> 
    <link rel="stylesheet" type="text/css" href="/font/flaticon.css"> 
</head>
<body ng-app="agc"  class="waitMe_body" id="wait-me">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Associated Gun Clubs',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
       <p class="pull-right">Powered by <a target="_blank" href="http://www.itekk.us/" rel="external">iTekk</a></p>

    </div>
</footer>

<?php $this->endBody() ?>
<script src="http://projects.itekk.us/associatedgunclubs/backend/web/js/sweetalert.min.js"></script>
<script src="http://projects.itekk.us/associatedgunclubs/backend/web/js/waitMe.js"></script>

<script type="text/javascript">

    $(".menu-box-parent .disabled").click(function(event) {
        event.preventDefault();
        sweetAlert("Sorry...", "will coming soon!", "error");

    })

    
    function run_waitMe(action){
        if(action=='show') {
            $('#wait-me').waitMe({
                effect: 'win8',
                text: 'Please wait...',
                bg: 'rgba(255,255,255,0.7)',
                color: '#00aff0',
                sizeW: '',
                sizeH: '',
                source: '',
                onClose: function() {}
            });
        }
        else if (action=='hide') {
            $(".waitMe").hide();
        }
    
    }
   
</script>

<script type="text/javascript">





$( document ).ready(function() {
    $("#postprinttransactionssearch-created_at").change(function() {
        $("#postPrintTransactionForm").submit();
    });

});
</script>


<script>
var app = angular.module('agc', []);
app.controller('FeesStructureForm', function($scope) {
    var type = $('div#feesstructure-type input:checked').val();
    if(type!='') {
        if(type=='badge_fee') {
            $(".form-group.field-feesstructure-membership_id").show();
        }
        else if (type=='certification') {
            $("#feesstructure-membership_id").val('');
            $(".form-group.field-feesstructure-membership_id").hide();
        }
    }

    $('div#feesstructure-type input').change(function() {
        var type = $('div#feesstructure-type input:checked').val();
        if(type=='badge_fee') {
            $(".form-group.field-feesstructure-membership_id").show();
        }
        else if (type=='certification') {
            $("#feesstructure-membership_id").val('');
            $(".form-group.field-feesstructure-membership_id").hide();
        }
    });

});



app.controller('CreateBadgeController', function($scope) {
    

    $( document ).ready(function() {

        $("#badges-badge_type").val($("#badges-mem_type").val());
            var memTypeId = $("#badges-mem_type").val();
            if (memTypeId!='') {
                run_waitMe('show');
                var responseData;
                jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/fee-structure/fees-by-type?id='+memTypeId,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData = JSON.parse(responseData);
                        $scope.fee = responseData;
                        $("#badges-badge_fee-disp").val(responseData.badgeFee);
                        $("#badges-badge_fee").val(responseData.badgeFee);
                        $("#badges-discounts-disp").val(responseData.discount);
                        $("#badges-discounts").val(responseData.discount);
                        $("#badges-amt_due-disp").val(responseData.badgeSpecialFee);
                        $("#badges-amt_due").val(responseData.badgeSpecialFee);
                        console.log(responseData);
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                }); 
                run_waitMe('hide');
            }
        
        $("#badges-discounts").change(function() {
            var badgeFee = $("#badges-badge_fee").val();
            var discount = $("#badges-discounts").val();
            var amountDue = badgeFee - discount;
            if(amountDue<0) {
                amountDue = 0.00;
            }
            $("#badges-amt_due-disp").val(amountDue);
            $("#badges-amt_due").val(amountDue);
        });

        $("#club-id").change(function() {
            $("#badges-club_id").val($("#club-id").val());
        });
        
        $("#badges-mem_type").change(function() {
            $("#badges-badge_type").val($("#badges-mem_type").val());
            var memTypeId = $("#badges-mem_type").val();
            if (memTypeId!='') {
                run_waitMe('show');
                var responseData;
                jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/fee-structure/fees-by-type?id='+memTypeId,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData = JSON.parse(responseData);
                        $scope.fee = responseData;
                        $("#badges-badge_fee-disp").val(responseData.badgeFee);
                        $("#badges-badge_fee").val(responseData.badgeFee);
                        $("#badges-discounts-disp").val(responseData.discount);
                        $("#badges-discounts").val(responseData.discount);
                        $("#badges-amt_due-disp").val(responseData.badgeSpecialFee);
                        $("#badges-amt_due").val(responseData.badgeSpecialFee);
                        console.log(responseData);
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                }); 
                run_waitMe('hide');
            }
             
        });

        family_badge_view('hide');
        $("div .field-primary-id").hide(); 
        $(".badges-form #badges-mem_type").change(function() {
            run_waitMe('show');
            var memTypeId = $("#badges-mem_type").val();
            if(memTypeId=='2') {
                family_badge_view('show');
            }
            else {
                family_badge_view('hide');
            }
            run_waitMe('hide');
        });


        function family_badge_view(action) {
            if(action=='show') {
                $("div .field-primary-id").show();
            }
            else if(action=='hide') {
                $("div .field-primary-id").hide();
            }
        }

    });
    
});

app.controller('RenewBadge', function($scope) { 
    $("#badgesubscriptions-discount").change(function() {
        var badgeFee = $("#badgesubscriptions-badge_fee").val();
        var discount = $("#badgesubscriptions-discount").val();
        var amountDue = badgeFee - discount;
        if(amountDue<0) {
            amountDue = 0;
        }
        if(discount>badgeFee) {
            discount = badgeFee;
        }
        $("#badgesubscriptions-discount").val(discount);
        $("#badgesubscriptions-amount_due").val(amountDue);

        jQuery.ajax({
            method: 'GET',
            url: '<?=yii::$app->params['rootUrl']?>/fee-structure/ajaxmoney-convert?value='+amountDue,
            crossDomain: false,
            success: function(responseData, textStatus, jqXHR) {
                responseData =  JSON.parse(responseData);
                $("#tableNetAmountDue").html(responseData.responce);

            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(responseData);
            },
        });

        jQuery.ajax({
            method: 'GET',
            url: '<?=yii::$app->params['rootUrl']?>/fee-structure/ajaxmoney-convert?value='+discount,
            crossDomain: false,
            success: function(responseData, textStatus, jqXHR) {
                responseData =  JSON.parse(responseData);
                $("#tableDiscount").html(responseData.responce);

            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(responseData);
            },
        });

    });


});

app.controller('UpdateBadgeController', function($scope) {
    

    $( document ).ready(function() {

        $("#club-id").change(function() {
            $("#badges-club_id").val($("#club-id").val());
        });

        $("#badgesubscriptions-discount").change(function() {
            var badgeFee = $("#badgesubscriptions-badge_fee").val();
            var discount = $("#badgesubscriptions-discount").val();
            var amountDue = badgeFee - discount;
            if(amountDue<0) {
                amountDue = 0.00;
            }
            if(discount>badgeFee) {
                discount = badgeFee;
            }
            $("#badgesubscriptions-amount_due").val(amountDue);
        });


        /*jQuery.ajax({
            method: 'GET',
            url: '<?=yii::$app->params['rootUrl']?>/badges/generate-new-sticker',
            crossDomain: false,
            success: function(responseData, textStatus, jqXHR) {
                responseData =  JSON.parse(responseData);
                $("#badgecertification-sticker").val(responseData.sticker);
                
            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(responseData);
            },
        });*/

        
       

        family_badge_view('hide');
        $("div .field-primary-id").hide(); 
        $(".badges-form #badges-mem_type").change(function() {
            run_waitMe('show');
            var memTypeId = $("#badges-mem_type").val();
            if(memTypeId=='2') {
                family_badge_view('show');
            }
            else {
                family_badge_view('hide');
            }
            run_waitMe('hide');
        });


        var memTypeId = $("#badges-mem_type").val();
            if(memTypeId!='') {
                collectRenewFee('fill',memTypeId);
            }
            else{
                collectRenewFee('remove');
            }


        function family_badge_view(action) {
            if(action=='show') {
                $("div .field-primary-id").show();
            }
            else if(action=='hide') {
                $("div .field-primary-id").hide();
            }
        }


        $("#badges-mem_type").change(function() {
            var memTypeId = $("#badges-mem_type").val();
            if(memTypeId!='') {
                collectRenewFee('fill',memTypeId);
            }
            else{
                collectRenewFee('remove');
            }
        });


        function collectRenewFee(action,memTypeId) {
            if(action=='fill') {
                jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/fee-structure/fees-by-type?id='+memTypeId,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData =  JSON.parse(responseData);
                        console.log(responseData);
                        $("#badgesubscriptions-badge_fee").val(responseData.badgeFee);

                        var badgeNumber = $("#badgesubscriptions-badge_number").val();
                        var badgeFee = $("#badgesubscriptions-badge_fee").val();
                        var credit = $("#badgesubscriptions-total_credit").val();


                        jQuery.ajax({
                            method: 'POST',
                            url: '<?=yii::$app->params['rootUrl']?>/badges/api-generate-renaval-fee',
                            crossDomain: false,
                            data: {'badgeNumber':badgeNumber,'BadgeFee': badgeFee,'credit':credit},
                            success: function(responseData, textStatus, jqXHR) {
                                responseData =  JSON.parse(responseData);
                                if(responseData.redeemableCredit=='') {
                                    responseData.redeemableCredit='0';
                                }
                                console.log(responseData);
                                $("#badgesubscriptions-redeemable_credit").val(responseData.redeemableCredit);
                                $("#badgesubscriptions-discount").val(responseData.discount);
                                $("#badgesubscriptions-amount_due").val(responseData.amountDue);

                            },
                            error: function (responseData, textStatus, errorThrown) {
                                console.log(responseData);
                            },
                        });
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                });

            }
             if(action=='remove') {
                $("#badgesubscriptions-badge_fee").val('');
                $("#badgesubscriptions-redeemable_credit").val('');
                $("#badgesubscriptions-discount").val('');
                $("#badgesubscriptions-amount_due").val('');
            }
        }

    });
    
});




app.controller('WorkTransferForm', function($scope) {

    var badgeNumber = $("#credittransferqueue-from_badge_number").val();
        if(badgeNumber=='') {
            changeBadgeName('remove');  
            $("#credit-block").hide();

            
        }

    $("#credittransferqueue-from_badge_number").change(function() {
        var badgeNumber = $("#credittransferqueue-from_badge_number").val();
        if(badgeNumber!='') {
            changeBadgeName('fill',badgeNumber);
            $("#credit-block").show(); 
        }
        else {
            changeBadgeName('remove');  
            $("#credit-block").hide();
            $("#credittransferqueue-total_credit_avl").val('');
        }
    });

    $("#credittransferqueue-to_badge_number").change(function() {
        var toBadgeNumber = $("#credittransferqueue-to_badge_number").val();

        if(toBadgeNumber!='') {
            changeBadgeNameTo('fill',toBadgeNumber);
        }
        else {
            changeBadgeNameTo('remove');
        }
    });



    function changeBadgeName(action,badgeNumber) {
        if(action=='fill') {
            jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/badges/get-badge-details?badge_number='+badgeNumber,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData =  JSON.parse(responseData);
                        $("#credittransferqueue-from_badge_name").val(responseData.first_name+' '+responseData.last_name);
                        $("#credittransferqueue-total_credit_avl").val(responseData.work_credits);
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                });
        }
        else if(action=='remove') {
            $("#credittransferqueue-from_badge_name").val('');
        }
    }

    function changeBadgeNameTo(action,badgeNumber) {
        if(action=='fill') {
            jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/badges/get-badge-details?badge_number='+badgeNumber,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData =  JSON.parse(responseData);
                        $("#credittransferqueue-to_badge_name").val(responseData.first_name+' '+responseData.last_name);
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                });
        }
        else if(action=='remove') {
            $("#credittransferqueue-to_badge_name").val('');
        }
    }

});


app.controller('ImportBadges', function($scope) { 

    $(document).ready(function (e) {
        $("#uploadFile").on('submit',(function(e) {
            e.preventDefault();
            $('#uploadingInfo').show();
            $.ajax({
                url: '<?=yii::$app->params['rootUrl']?>/badges/import-badges', // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,      // To send DOMDocument or non processed data file it is set to false
                    xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    console.log(percentComplete);
                                    //$('.myprogress').text(percentComplete + '%');
                                    //$('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                    },
                     success: function(responseData, textStatus, jqXHR) {
                        responseData =  JSON.parse(responseData);
                        if(responseData.status=='stoped') {
                            $("#uploadingInfo").hide();
                            $("#uploadingInfoError").show();
                            $("#erroCount").html(responseData.count);
                        }
                        if(responseData.status=='success') {
                            $("#uploadingInfo").hide();
                            $(".sucessbox").html('<idv class="col-xs-12""> <div class="alert alert-success alert-dismissable fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong> Importing Summary</strong>  <br> <ul> <li> New Successful Imports : '+responseData.successCount+'  </li>  <li> Re Requested Badges Already in system : '+responseData.errorCount+'  </li>  </ul></div> </idv>');
                            console.log(responseData);
                        }
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
            });
    }));

    });

});




app.controller('ImportWorkCredits', function($scope) { 

    $(document).ready(function (e) {
        $("#uploadFile").on('submit',(function(e) {
            e.preventDefault();
            $('#uploadingInfo').show();
            $.ajax({
                url: '<?=yii::$app->params['rootUrl']?>/work-credits/import', // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,      // To send DOMDocument or non processed data file it is set to false
                    xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    console.log(percentComplete);
                                    //$('.myprogress').text(percentComplete + '%');
                                    //$('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                    },
                     success: function(responseData, textStatus, jqXHR) {
                        $('#uploadingInfo').hide();
                        responseData =  JSON.parse(responseData);
                        var corrupted = responseData.corrupted;
                        var errorArrayNotExist = responseData.errorArrayNotExist;
                        var successful = responseData.successful;
                        $(".sucessbox").html('<idv class="col-xs-12""> <div class="alert alert-success alert-dismissable fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong> Importing Summary </strong>  <br> <ul> <li> Successfully completed records : '+responseData.successful +'  </li>  <li> Corrupted Rows : '+responseData.corrupted+'  </li>  <li> Undefined Badge Numbers count : '+responseData.errorArrayNotExist +'  </li>   </ul></div> </idv>');
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        $('#uploadingInfo').hide();
                        $(".sucessbox").html('<idv class="col-xs-12""> <div class="alert alert-danger alert-dismissable fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong> Error </strong> an error occurred while processing your request pleae upload a valid formated data. </div> </idv>');
                    },
            });
    }));

    });

});


app.controller('BadgeCreditFrom', function($scope) {
    var badgeNumber;
    badgeNumber =  $("#workcredits-badge_number").val();
    if(badgeNumber!='') {
        changeBadgeName('fill',badgeNumber);
    }
    else {
        changeBadgeName('remove');  
    }

    $("#workcredits-badge_number").change(function() {
        badgeNumber =  $("#workcredits-badge_number").val();
        if(badgeNumber!='') {
            changeBadgeName('fill',badgeNumber);
        }
        else {
          changeBadgeName('remove');  
        }
    });

    function changeBadgeName(action,badgeNumber) {
        if(action=='fill') {
            jQuery.ajax({
                    method: 'GET',
                    url: '<?=yii::$app->params['rootUrl']?>/badges/get-badge-details?badge_number='+badgeNumber,
                    crossDomain: false,
                    success: function(responseData, textStatus, jqXHR) {
                        responseData =  JSON.parse(responseData);
                        $("#workcredits-badge_holder_name").val(responseData.first_name+' '+responseData.last_name);
                        
                    },
                    error: function (responseData, textStatus, errorThrown) {
                        console.log(responseData);
                    },
                });
        }
        else if(action=='remove') {
            $("#workcredits-badge_holder_name").val('');
        }
    }


    $(".next-Credit").click(function(e) {
        e.preventDefault();
        jQuery.ajax({
            method: 'GET',
            url: '<?=yii::$app->params['rootUrl']?>/work-credits/sticky-form?type=true',
            crossDomain: false,
            success: function(responseData, textStatus, jqXHR) {
                $("form#creditEntryForm").submit();  
            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(responseData);
            },
        }); 
    });

    $(".done-Credit").click(function(e) {
        e.preventDefault();
        jQuery.ajax({
            method: 'GET',
            url: '<?=yii::$app->params['rootUrl']?>/work-credits/sticky-form?type=false',
            crossDomain: false,
            success: function(responseData, textStatus, jqXHR) {
                $("form#creditEntryForm").submit();  
            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(responseData);
            },
        }); 
    });


});







</script>

</body>
</html>
<?php $this->endPage() ?>
