<?php
/* @var $this yii\web\View */
?>


<h1>work credits</h1>
<a href="/badge/work-credit-entry" class="btn btn-success pull-right"> Add Credit</a>
<div id="w0" class="grid-view">
	<div class="summary">
		Showing <b>1-9</b> of <b>10</b> items.
	</div>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>	<a href="" data-sort="">Badge Number</a>
				</th>
				<th>	<a href="" data-sort="">Work Date</a>
				</th>
				<th>	<a href="" data-sort="">Project</a> </th>
				<th>	<a href="" data-sort="">Workhours</a> </th>	
				<th>	<a href="" data-sort="">Auth</a> </th>
				<th>	<a href="" data-sort="">status</a> </th>
				<th>	<a href="" data-sort="">Remarks</a> </th>
				<th>	<a href="" data-sort="">Procdate</a> </th>
				<th>	<a href="" data-sort="">Who</a> </th>
			</tr>
			<tr id="w0-filters" class="filters">
				<td>&nbsp;</td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>
				<td><input type="text" class="form-control" name=""></td>

				<td><input type="text" class="form-control" name=""></td>
			</tr>
		</thead>
		<tbody>
			<tr data-key="4509">
				<td>1</td>
				<td>0001</td>
				<td> 04-01-2016 </td>
				<td>Range Day 1</td>
				<td>4</td>
				<td> EVP </td>
				<td> BULK IMP </td>
				<td> memo </td>
				<td>04-06-2016</td>
				<td>Admin</td>
				
			</tr>

			<tr data-key="4509">
				<td>2</td>
				<td>0002</td>
				<td> 04-01-2016 </td>
				<td>Range Day 1</td>
				<td>4</td>
				<td> EVP </td>
				<td> BULK IMP </td>
				<td> memo </td>
				<td>04-06-2016</td>
				<td>Admin</td>
				
			</tr>


			<tr data-key="4509">
				<td>3</td>
				<td>0003</td>
				<td> 04-01-2016 </td>
				<td>Range Day 1</td>
				<td>4</td>
				<td> EVP </td>
				<td> BULK IMP </td>
				<td> memo </td>
				<td>04-06-2016</td>
				<td>Admin</td>
				
			</tr>
			
			<tr data-key="4509">
				<td>4</td>
				<td>0004</td>
				<td> 04-01-2016 </td>
				<td>Range Day 1</td>
				<td>4</td>
				<td> EVP </td>
				<td> BULK IMP </td>
				<td> memo </td>
				<td>04-06-2016</td>
				<td>Admin</td>
				
			</tr>

			<tr data-key="4509">
				<td>5</td>
				<td>0005</td>
				<td> 04-01-2016 </td>
				<td>Range Day 1</td>
				<td>4</td>
				<td> EVP </td>
				<td> BULK IMP </td>
				<td> memo </td>
				<td>04-06-2016</td>
				<td>Admin</td>
				
			</tr>
			

		</tbody>
	</table>
</div>