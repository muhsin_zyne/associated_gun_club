<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
?>

<div class="row">
	<div class="col-xs-12">
		<img src="/images/header.png" style="width: 100%">
	</div>
</div>
<div style="padding: 20px 60px">
	<div class="row">

		<div>
			<span style="font-size: 16px; color: #3c3131; font-weight: bold;"> Badge Number : 100001</span>
			<img  style="padding-left: 320px" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=10030305">
		</div>

        <div class="col-xs-12 table-responsive">

          <table class="table table-striped">
            
		<tbody>
            
            <tr>
              <th> Badge Number </th>
              <td> 100001 </td>
              <th> Badge Type </th>
              <td> Primary </td>
            </tr>
            <tr>
              <th> First Name </th>
              <td> Martin </td>
              <th> Last Name  </th>
              <td> G Amit </td>
            </tr>
            <tr>
              <th> Gender </th>
              <td> Male </td>
              <th> YOB  </th>
              <td> 21-01-1988 </td>
            </tr>
            <tr>
              <th> City </th>
              <td> Test City </td>
              <th> State  </th>
              <td> Test State </td>
            </tr>
            <tr>
              <th> Phone </th>
              <td> +41 5993 333 334 </td>
              <th> Email  </th>
              <td> itekk.300@yopmail.com </td>
            </tr>

             <tr>
              <th> Club Name </th>
              <td> Anne Aundel Country County Gun Club </td>
              <th>   </th>
              <td>  </td>
            </tr>

            <tr>
              <th> Valid From </th>
              <td> 22-Mar-2018 </td>
           		<th> Valid True </th>
              	<td> 21-Sep-2018  </td>
            	</tr>

           

            
            

            
            
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
</div>
           

