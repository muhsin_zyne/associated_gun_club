<?php
/* @var $this yii\web\View */
?>
<h2>badge / index</h2>

<div id="w0" class="grid-view">
	<div class="summary">
		Showing <b>1-9</b> of <b>10</b> items.
	</div>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>	<a href="" data-sort="">Badge Number</a>
				</th>
				<th>	<a href="" data-sort="">Badge Type</a>
				</th>

				<th>	<a href="" data-sort="">Gender</a> </th>	
				<th>	<a href="" data-sort="">First Name</a> </th>	
				<th>	<a href="" data-sort="">Action</a> </th>	
			</tr>
			<tr id="w0-filters" class="filters">
				<td>&nbsp;</td>
				<td><input type="text" class="form-control" name="InvoiceSearch[invoice_id]"></td>
				<td><select id="badges-membertype" class="form-control" name="Badges[memberType]">
					<option value="">select</option>
					<option value="1">Primary</option>
					<option value="2">Family</option>
					<option value="3">Junior</option>
					<option value="4">Life</option>
					</select> </td>
				<td><select id="badges-gender" class="form-control" name="Badges[gender]">
				<option value="">select</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
				</select></td>
				<td><input type="text" class="form-control" name="InvoiceSearch[customer_id]"></td>

				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<tr data-key="4509">
				<td>1</td>
				<td>0001</td>
				<td>Primary</td>
				<td>Male</td>
				<td>Jhon</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4510">
				<td>2</td>
				<td>0002</td>
				<td>Primary</td>
				<td>Female</td>
				<td>Tessa</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>
			<tr data-key="4509">
				<td>3</td>
				<td>0003</td>
				<td>Family</td>
				<td>Male</td>
				<td>Hammy M</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>4</td>
				<td>0004</td>
				<td>Primary</td>
				<td>Male</td>
				<td>Ben N</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>5</td>
				<td>0005</td>
				<td>Primary</td>
				<td>Female</td>
				<td>Miya J</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>6</td>
				<td>0006</td>
				<td>Primary</td>
				<td>Male</td>
				<td>Nash CU</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>7</td>
				<td>0007</td>
				<td>Primary</td>
				<td>Male</td>
				<td>Alby K</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>8</td>
				<td>0008</td>
				<td>Primary</td>
				<td>Male</td>
				<td>Martin G</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>

			<tr data-key="4509">
				<td>9</td>
				<td>0009</td>
				<td>Primary</td>
				<td>Female</td>
				<td>Amber K</td>
				<td>
					<a href="/badge/badge-print-view" target="_blank" title="Print" aria-label="Update" data-pjax="0"> <i class="fa fa-print" aria-hidden="true"></i> </a>
					<a href="" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
					<a href="/badge/update" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				</td>
			</tr>
			
			
			
			

		</tbody>
	</table>
</div>