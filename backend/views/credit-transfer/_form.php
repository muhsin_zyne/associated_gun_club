<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CreditTransferQueue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credit-transfer-queue-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from_badge_number')->textInput() ?>

    <?= $form->field($model, 'to_badge_number')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'work_hours')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'init' => 'Init', 'success' => 'Success', 'rejected' => 'Rejected', 'pending' => 'Pending', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'approved_by')->textInput() ?>

    <?= $form->field($model, 'approved_at')->textInput() ?>

    <?= $form->field($model, 'created_role')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
