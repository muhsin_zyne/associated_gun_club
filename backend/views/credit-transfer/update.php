<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CreditTransferQueue */

$this->title = 'Update Credit Transfer Queue: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credit Transfer Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="credit-transfer-queue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
