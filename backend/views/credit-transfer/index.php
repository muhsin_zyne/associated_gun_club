<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CreditTransferQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Credit Transfer Queues';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-transfer-queue-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Credit Transfer Queue', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'from_badge_number',
            'to_badge_number',
            'created_at',
            'work_hours',
            // 'status',
            // 'note:ntext',
            // 'created_by',
            // 'approved_by',
            // 'approved_at',
            // 'created_role',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
