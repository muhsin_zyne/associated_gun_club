<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CreditTransferQueue */

$this->title = 'Create Credit Transfer Queue';
$this->params['breadcrumbs'][] = ['label' => 'Credit Transfer Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-transfer-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
