<?php

namespace backend\controllers;

use Yii;
use backend\models\WorkCredits;
use backend\models\search\WorkCreditsSearch;
use backend\models\CreditTransferQueue;
use backend\models\WorkCreditTransactions;
use backend\models\search\CreditTransferQueueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;
use backend\models\Badges;

/**
 * WorkCreditsController implements the CRUD actions for WorkCredits model.
 */
class WorkCreditsController extends SiteController
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkCredits models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkCreditsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorkCredits model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WorkCredits model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionStickyForm($type) {
        if($type=='true') {
            $session = Yii::$app->session;
            $session->set('stickyForm', 'true');
            $responce = [
                'status'=>true,
            ];

            return json_encode($responce,true);
        }
        else if($type=='false') {
            $session = Yii::$app->session;
            $session->set('stickyForm', 'false');
            $responce = [
                'status'=>false,
            ];

            return json_encode($responce,true);
        }
    }

    public function actionImport() {
        if(yii::$app->request->post()) {
            $activeUser = $this->getActiveUser();
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time',6000000000);
            $workCredits = $_FILES['file']['tmp_name'];
            $objPHPExcel = new \PHPExcel();
            $items = Excel::import($workCredits);
            $errorArrayNotExist = [];
            $corrupted = [];
            $successful = 0;
            foreach ($items as $key => $item) {
                if(isset($item['badgenum']) && $item['badgenum']!=null) {
                    $isExist = $this->badgeIsExist($item['badgenum']);
                    if($isExist==true) {
                        $workCreditsModel = new WorkCredits();
                        $workCreditsModel->badge_number = $item['badgenum'];
                        $workCreditsModel->work_date = date('Y-m-d',strtotime($item['workdate']));
                        $workCreditsModel->work_hours = $item['workhours'];
                        $workCreditsModel->project_name = $item['project'];
                        $workCreditsModel->status = '0';
                        $workCreditsModel->remarks = $item['remarks']!=null ? $item['remarks']: '-blank-';
                        $workCreditsModel->authorized_by = $item['who'];
                        $workCreditsModel->created_at = $this->getNowTime();
                        $workCreditsModel->updated_at = $this->getNowTime();
                        $workCreditsModel->created_role = $activeUser->privilege;
                        if($workCreditsModel->save()) {
                            $badge = Badges::findOne($workCreditsModel->badge_number);
                            $badge->work_credits = $badge->work_credits + $workCreditsModel->work_hours;
                            if($badge->save(false)) {
                                $workCreditsModel->status = '1';
                                $workCreditsModel->save();
                                $workCreditTransactions = new WorkCreditTransactions();
                                $workCreditTransactions->badge_number = $workCreditsModel->badge_number;
                                $workCreditTransactions->type = 'debit';
                                $workCreditTransactions->value = $workCreditsModel->work_hours;
                                $workCreditTransactions->remarks = 'Work Credit Add - '.$workCreditsModel->remarks;
                                $workCreditTransactions->created_at = $this->getNowTime();
                                $workCreditTransactions->updated_at = $this->getNowTime();
                                $workCreditTransactions->work_credits_id = $workCreditsModel->id;
                                if($workCreditTransactions->save()) {

                                }
                             }
                        $successful++;

                        }
                    }
                    else {
                        $errorArrayNotExist [] = $item;
                    }

                }
                else {
                    $corrupted [] = $item;
                }
                
            }

            $responce = [
                'errorArrayNotExist' => count($errorArrayNotExist),
                'corrupted' => count($corrupted),
                'successful' => $successful,
            ];

            return json_encode($responce,true);

        }
        else {
            return $this->render('import-form',[
            ]);
        }
    }


    public function actionCreate()
    {   
        $model = new WorkCredits();

        

        if ($model->load(Yii::$app->request->post())) {

            $session = Yii::$app->session;
            $sticky = $session->get('stickyForm');
            if($sticky=='true') {
                $stickyData = [
                    'work_date'=> date('Y-m-d',strtotime($model->work_date)),
                    'authorized_by' => $model->authorized_by,
                    'work_hours' => $model->work_hours,
                    'project_name' => $model->project_name,
                    'remarks' => $model->remarks,

                ];
                $session->set('stickyData',$stickyData);
            }

            else if($sticky=='false') {
               $stickyData = [];
               $session->set('stickyData',$stickyData);
            }

            $activeUser = $this->getActiveUser();
            $model->work_date = date('Y-m-d',strtotime($model->work_date));
            $model->status = '0';
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();
            $model->created_role = $activeUser->privilege;
            if($model->save()) {
                 $badge = Badges::findOne($model->badge_number);
                 $badge->work_credits = $badge->work_credits + $model->work_hours;
                 if($badge->save(false)) {
                    $model->status = '1';
                    $model->save();
                    $workCreditTransactions = new WorkCreditTransactions();
                    $workCreditTransactions->badge_number = $model->badge_number;
                    $workCreditTransactions->type = 'debit';
                    $workCreditTransactions->value = $model->work_hours;
                    $workCreditTransactions->remarks = 'Work Credit Add - '.$model->remarks;
                    $workCreditTransactions->created_at = $this->getNowTime();
                    $workCreditTransactions->updated_at = $this->getNowTime();
                    $workCreditTransactions->work_credits_id = $model->id;
                    if($workCreditTransactions->save()) {

                    }
                 }
                Yii::$app->getSession()->setFlash('success', 'Work Credit has been added');
                $sticky = $session->get('stickyForm');
                if($sticky=='true') {
                    return $this->redirect(['/work-credits/create']);
                }
                else if($sticky=='false') {
                 return $this->redirect(['view', 'id' => $model->id]);   
                }
                
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {



            if(isset($_GET['badge_number'])) {
                $badgeArray = Badges::findOne($_GET['badge_number']);

            }
            else {
                $badgeArray = null;
            }

            return $this->render('create', [
                'model' => $model,
                'badgeArray' =>$badgeArray,
            ]);
        }
    }

    /**
     * Updates an existing WorkCredits model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $activeUser = $this->getActiveUser();
            $badge = Badges::findOne($model->badge_number);
            $badge->work_credits = $badge->work_credits - $model->work_hours;
            if($badge->save(false)) {
                $model->work_hours = $model->work_hours_new;
                $model->work_date = date('Y-m-d',strtotime($model->work_date));
                $model->updated_at = $this->getNowTime();
                $model->created_role = $activeUser->privilege;
                if($model->save()) {

                    $workCreditTransactions =WorkCreditTransactions::find()->where(['work_credits_id'=>$model->id])->one();
                    $workCreditTransactions->value = $model->work_hours;
                    $workCreditTransactions->remarks = 'Work Credit Add (updated) - '.$model->remarks;
                    $workCreditTransactions->updated_at = $this->getNowTime();
                    if($workCreditTransactions->save()) {

                    }

                    $badge->work_credits = $badge->work_credits + $model->work_hours;
                    if($badge->save(false)) {
                        $model->status = '1';
                        $model->save();
                        Yii::$app->getSession()->setFlash('success', 'Work Credit has been updated');
                        return $this->redirect(['view', 'id' => $model->id]);   
                    }
                    
                }
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WorkCredits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {    
        $deleteModel = WorkCredits::findOne($id);
        $badge = Badges::findOne($deleteModel->badge_number);
        if(empty($badge)) {
            Yii::$app->getSession()->setFlash('error', 'Opps! Could not find a valid badge associated with this work credit. Badge May be removed earlier from the system');
            return $this->redirect(['index']); 
        }
        $badge->work_credits = $badge->work_credits - $deleteModel->work_hours;
        if($badge->save(false)) {

            $workCreditTransactions = new WorkCreditTransactions();
            $workCreditTransactions->badge_number = $deleteModel->badge_number;
            $workCreditTransactions->type = 'credit';
            $workCreditTransactions->value = $deleteModel->work_hours;
            $workCreditTransactions->remarks = 'Work credit removed from account';
            $workCreditTransactions->created_at = $this->getNowTime();
            $workCreditTransactions->updated_at = $this->getNowTime();
            if($workCreditTransactions->save()) {

            }

            if($deleteModel->delete()) {
                Yii::$app->getSession()->setFlash('success', 'Work Credit has been deleted');
                return $this->redirect(['index']);
            }
            
        }

        
    }

    public function actionTransferForm() {
        $activeUser = $this->getActiveUser();
        $model = new CreditTransferQueue();
        $WorkCreditsModel  = new WorkCredits();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = $this->getNowTime();
            $model->status = 'init';
            $model->created_by = $activeUser->id;
            $model->approved_at = null;
            $model->created_role = $activeUser->privilege;

            if($model->validate()) {
                $model->save();
                Yii::$app->getSession()->setFlash('success', 'credit transfer request has been initiated');
                return $this->redirect(['/work-credits/credit-transfer','type'=>'init']);
                
            }
             
        }

        return $this->render('transfer-form',[
            'model'=>$model,
            'WorkCreditsModel'=> $WorkCreditsModel,
        ]);
            
    }

    public function actionCreditTransfer($type) {

        $searchModel = new CreditTransferQueueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($type=='init') {
            $dataProvider->query->andWhere(['status'=>'init']);
        }
        else if($type=='success') {
            $dataProvider->query->andWhere(['status'=>'success']);
        }


        return $this->render('credit-transfer',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
        ]);
    }

    public function actionTransferView($request_id) {

        $model = CreditTransferQueue::findOne($request_id);

        return $this->render('transfer-view',[
            'model'=>$model
            ]);
    }

    public function actionTransferConfirm($id) {
        if(Yii::$app->request->post()) {
            $approveModel = CreditTransferQueue::findOne($id);
            if($approveModel->status!='success') {
                $transferRequestResponce = $approveModel->creditTransferRequest($approveModel->from_badge_number,$approveModel->work_hours);
                if($transferRequestResponce['status']==true) {
                    $doTransaction = $approveModel->doTransferCredit($approveModel->from_badge_number,$approveModel->to_badge_number,$approveModel->work_hours);
                    if($doTransaction==true) {
                        $approveModel->status ='success';
                        $approveModel->approved_at = $this->getNowTime();
                        if($approveModel->save(false)) {
                            Yii::$app->getSession()->setFlash('success', 'credit transfer request has been Approved');
                            return $this->redirect(['/work-credits/transfer-view','request_id'=>$approveModel->id]);
                        }
                        
                    }
                    else if($doTransaction==false){
                        Yii::$app->getSession()->setFlash('error', 'Insufficient credit in doner account or doner account credit may expired');
                        return $this->redirect(['/work-credits/transfer-view','request_id'=>$approveModel->id]);
                    }
                }
                else {
                    Yii::$app->getSession()->setFlash('error', 'Insufficient credit in doner account or doner account May expired');
                        return $this->redirect(['/work-credits/transfer-view','request_id'=>$approveModel->id]);

                }

            }
            else {
                
                Yii::$app->getSession()->setFlash('error', 'Sorry work credit has been already Approved');
                return $this->redirect(['/work-credits/transfer-view','request_id'=>$approveModel->id]);
            }

            

        }
    }  

    /**
     * Finds the WorkCredits model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkCredits the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function badgeIsExist($badge_number) {
        $badges  = Badges::find()->where(['badge_number'=>$badge_number])->one();
        if(!empty($badges)) {
            return true;
        }
        else {
            return false;
        }
    }

    protected function findModel($id)
    {
        if (($model = WorkCredits::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
