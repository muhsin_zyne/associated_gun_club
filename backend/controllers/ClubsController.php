<?php

namespace backend\controllers;

use Yii;
use backend\models\Clubs;
use backend\models\search\ClubsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;
use backend\models\search\BadgesRosterSearch;
/**
 * ClubsController implements the CRUD actions for Clubs model.
 */
class ClubsController extends SiteController
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionImportForm() {
        
        if(yii::$app->request->post()) {


            $fileNameTest = $_FILES['file']['tmp_name'];
            $objPHPExcel = new \PHPExcel();
            $items = Excel::import($fileNameTest);
            
            $errorRows = [];
            $batchInsertArray = [];
            $successCount = 0;
            foreach ($items as  $item) {
                $model = new Clubs;
                $model->club_id = $item['club id'];
                $model->club_name = $item['club name'];
                $model->short_name = $item['short name'];
                $model->status = '0';
                if($model->validate()) {
                    $model->save();
                    $successCount++;
                }
                else {
                    $errorRows [] = $model;
                }
                
            }

            if(!empty($errorRows)) { 
                $errorArray = [];
                foreach ($errorRows as $modelError) {
                    $errorArray [] = [
                        'club_id' => $modelError->club_id,
                        'club_name' => $modelError->club_name,
                        'short_name' => $modelError->short_name,
                    ];
                }
                if($successCount>0) {
                    Yii::$app->getSession()->setFlash('success', 'Imported '.$successCount.' items'); 
                }
                else if($successCount==0) {
                    Yii::$app->getSession()->setFlash('error', '0 New Clubs'); 
                }
               return $this->render('import-form',['errorArray'=>$errorArray,'successCount'=>$successCount]); 
            }
            else {

                Yii::$app->getSession()->setFlash('success', 'Imported '.$successCount.' items'); 
                return $this->render('import-form',['successCount'=>$successCount]); 
            }

            
        }        

        return $this->render('import-form',[]);
    
        
    }


    public function actionBadgeRosters() {

        $searchModel = new BadgesRosterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($dataProvider->query->where['club_id']!='undefined') {
            $club_id = $dataProvider->query->where['club_id'];
            $clubArray = Clubs::find()->where(['id'=>$club_id])->one();
            $exportFileName = 'Badge Roster - '.$clubArray->club_name;

        }
        else {
            $exportFileName = 'Badge Roster - Empty';
        }

        $expiredBefore = date('Y-m-d',strtotime("-5 year", strtotime($this->getNowTime())));
        $expireAfter = date('Y-m-d',strtotime("+20 year", strtotime($this->getNowTime())));
        
        //echo'<pre>'; print_r($expireAfter); die();    
        $dataProvider->query->andWhere(['between','expires',$expiredBefore, $expireAfter]);
        
        // /echo'<pre>'; print_r($dataProvider); die();
        $dataProvider->pagination->pageSize = 10;
        return $this->render('badge-rosters',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'exportFileName' => $exportFileName,
        ]);
        
        

        //echo'<pre>'; print_r(   "dssdssdds"); die();
    }





    /**
     * Lists all Clubs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClubsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clubs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clubs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clubs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->getSession()->setFlash('success', 'Club '.$model->short_name.' has been created');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Clubs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Club '.$model->short_name.' has been updated');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Clubs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        $deleteModel = Clubs::findOne($id);
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Club '.$deleteModel->short_name.' has been updated');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clubs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clubs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clubs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
