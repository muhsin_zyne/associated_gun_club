<?php

namespace backend\controllers;

use Yii;
use backend\models\Badges;
use backend\models\search\BadgesSearch;
use backend\models\BadgeSubscriptions;
use backend\models\WorkCreditTransactions;
use backend\models\MembershipType;
use backend\models\search\BadgeCertificationSearch;
use backend\models\search\WorkCreditTransactionsSearch;
use backend\models\search\BadgeSubscriptionsSearch;
use backend\models\search\PostPrintTransactionsSearch;
use backend\models\Clubs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use backend\controllers\AdminController;
use backend\models\WorkCredits;
use backend\models\search\WorkCreditsSearch;
use backend\models\FeesStructure;
use backend\models\BadgeCertification;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


/**
 * BadgesController implements the CRUD actions for Badges model.
 */
class BadgesController extends AdminController
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Badges models.
     * @return mixed
     */

    /*public function actionTest() {

        $searchModel = new BadgesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_test',[
                'searchModel'=> $searchModel,
                'dataProvider'=> $dataProvider,
            ]);





    }*/



    public function actionViewCertificate($membership_id,$view_id) {
       
       $badgesModel = Badges::findOne($membership_id);
       $certificationModel = BadgeCertification::findOne($view_id);

        return $this->render('view-certificate',[
            'badgeModel'=> $badgesModel,
            'certificationModel'=> $certificationModel,
        ]);
    }

    public function actionViewCertificationsList($id) {
        $searchModel = new BadgeCertificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['badge_number'=>$id]);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $this->render('view-certifications-list',[
            'model'=>$this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

     public function actionUpdateCertificate($membership_id,$view_id) {
       
       $badgesModel = Badges::findOne($membership_id);
       $certificationModel = BadgeCertification::findOne($view_id);
       if($certificationModel->load(Yii::$app->request->post())) {
            $certificationModel->updated_at = $this->getNowTime();
            $certificationModel->save(false);
            Yii::$app->getSession()->setFlash('success', 'Certificate has been edited');
            
       }

        return $this->render('update-certificate',[

            'badgeModel'=> $badgesModel,
            'certificationModel'=> $certificationModel,
        ]);
    }

    public function actionDeleteCertificate($membership_id,$view_id) {
        $certificationModel = BadgeCertification::findOne($view_id);
        if($certificationModel->badge_number==$membership_id) {
            $certificationModel->delete();
            Yii::$app->getSession()->setFlash('success', 'Certificate has been deleted');
            return $this->redirect(['badges/view-certifications-list','id'=>$membership_id]); 
        }
        //echo'<pre>'; print_r($certificationModel); die();

    }





    public function actionPostPrintTransactions() {

        $searchModel = new PostPrintTransactionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('post-print-transactions',[
                'searchModel'=> $searchModel,
                'dataProvider' => $dataProvider,
            ]);



    }


    public function actionPrintView($id) {
        $badgeModel = Badges::findOne($id);
        $content = $this->renderPartial('_badge-print-view',['model'=>$badgeModel]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_BLANK, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content, 
            'marginTop'=>0,
            'marginLeft'=>0,
            'marginRight'=>0,
            'marginBottom'=>0,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'AGC Range Badge'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Powered By itekk.us'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionGetFamilyBadges() {

        
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $clubId = $parents[0];
                $familyBadges = Badges::find()->where(['club_id'=>$clubId])->all();
                $familyBadgesList = ArrayHelper::map($familyBadges, 'badge_number','first_name');

                $array = [];

                foreach ($familyBadges as  $value) {
                    $array [] = [
                        'id' => $value->badge_number,
                        'name' => $value->badge_number.' - '.$value->first_name,
                    ];
                }

                echo Json::encode(['output'=>$array, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionIndex()
    {
        $searchModel = new BadgesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Badges model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionViewWorkCredits($id) {
        $searchModel = new WorkCreditsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['badge_number'=>$id]);
        $dataProvider->pagination->pageSize = 20;

        return $this->render('view-work-credits',[
                'model'=>$this->findModel($id),
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider,
        ]);
    }

    public function actionViewSubscriptions($id) {

        $model = $this->findModel($id);
        $subciptionsArray = BadgeSubscriptions::findOne($model->badge_subscription_id);
        return $this->render('view-subscriptions',[
                'model'=>$this->findModel($id),
                'subciptionsArray'=>$subciptionsArray,
            ]);
    }

    /**
     * Creates a new Badges model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionRenewMembership($membership_id) {
        $model = new BadgeSubscriptions();

        $badgeRecords = Badges::findOne($membership_id);
        $paymentArray = FeesStructure::find()->where([
                            'membership_id'=>$badgeRecords->badge_type,
                            'type'=>'badge_fee',
                            'status'=>'0'
                        ])->one();
        
        if ($model->load(Yii::$app->request->post())) {
                
                $model->valid_from = date('Y-m-d',strtotime($this->getNowTime()));
                $model->valid_true = date('Y-m-d',strtotime($model->expires));
                $model->status = 'active';
                $model->created_at = $this->getNowTime();
                $model->badge_fee = $model->badge_fee;
                $model->paid_amount = $model->badge_fee - $model->discount;
                //echo'<pre>'; print_r($model); die();
                $model->transaction_type = $model->redeemable_credit>0 ? 'CERT' : 'RENEW';
                $model->club_id = $badgeRecords->club_id;
                //echo'<pre>'; print_r($model->transaction_type); die();
                if($model->save()) {
                    $tempSubcriptionId = $badgeRecords->badge_subscription_id;
                    $badgeRecords->expires = $model->valid_true;
                    $badgeRecords->work_credits = $badgeRecords->work_credits - $model->redeemable_credit;
                    $badgeRecords->badge_subscription_id = $model->id;
                    $badgeRecords->sticker = $model->sticker;
                    if($model->redeemable_credit>0) {

                        $workCreditTransactions = new WorkCreditTransactions();
                        $workCreditTransactions->badge_number = $badgeRecords->badge_number;
                        $workCreditTransactions->type = 'credit';
                        $workCreditTransactions->value = $model->redeemable_credit;
                        $workCreditTransactions->remarks = 'Redeemed for renewal badge';
                        $workCreditTransactions->created_at = $this->getNowTime();
                        $workCreditTransactions->updated_at = $this->getNowTime();
                        $workCreditTransactions->save(); 

                    }
                    
                    if($badgeRecords->save(false)) {
                        $oldSubcriptionStatus = $this->expireBadgeSubcription($tempSubcriptionId);
                        Yii::$app->getSession()->setFlash('success', 'Membership has been Renewd');
                        return $this->redirect(['badges/view-subscriptions', 'id' => $model->badge_number]);
                    }
                }
                else {

                    $errors = $model->getErrors();
                    if(array_key_exists('sticker', $errors)) {
                        Yii::$app->getSession()->setFlash('error', 'Sticker '.$model->sticker.' has already been taken.');
                        return $this->redirect(['/badges/update', 'id' => $membership_id]);
                    }
                }



        }

        else {

            return $this->render('renew-membership',[
            'model'=>$model,
            'badgeRecords'=>$badgeRecords,
            'paymentArray'=>$paymentArray,
            ]);
        }

        
    }

    public function actionViewRenewalHistory($membership_id) {
        $model = Badges::findOne($membership_id);
        $searchModel = new BadgeSubscriptionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['badge_number'=>$membership_id]);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $this->render('view-renewal-history',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
        ]);

    }


    public function actionViewWorkCreditsLog($membership_id) {
        $model = Badges::findOne($membership_id);
        $searchModel = new WorkCreditTransactionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['badge_number'=>$membership_id]);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $this->render('view-work-credits-log',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
        ]);

    }

    public function actionImportBadges() {

        if(yii::$app->request->post()) {

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time',6000000000);
            $badgesFile = $_FILES['file']['tmp_name'];
            $objPHPExcel = new \PHPExcel();
            $items = Excel::import($badgesFile);
            if(count($items)>=2000) {
                $responce = [
                    'status'=>'stoped',
                    'remarks'=>'file size only limits to 3000 per',
                    'count' => count($items),

                ];

                return json_encode($responce,true);
            }
            $errorArray = [];
            foreach ($items as $key => $item) {
                $isExist = Badges::find()->where(['badge_number'=>$item['badgenum']])->all();
               if(empty($isExist)) {

                    $model = new Badges();
                    $badgeCertification = new BadgeCertification();
                    if($item['sticker']==null || $item['sticker']!=null) {
                        if($item['sticker']!=null) {
                            $sticker = $item['sticker'];
                        }
                        else {
                            $sticker = yii::$app->params['stickerPre'].rand(1111111,9999999999999); 
                        }
                        $responce = $badgeCertification->validateSticker($sticker);
                        if($responce==false) {
                            $responceStatus= false;
                            while ($responceStatus==false) {
                                $sticker = yii::$app->params['stickerPre'].rand(1111111,9999999999999);
                                $responce = $badgeCertification->validateSticker($sticker);
                                if($responce==true) {
                                    $responceStatus=true;
                                }
                            }
                        }
                        $model->sticker = $sticker;
                    }
                    
                    $model->badge_number = $item['badgenum'];
                    $model->prefix = $item['prefix'];
                    $model->first_name = $item['firstname'];
                    $model->last_name = $item['lastname'];
                    $model->suffix = $item['suffix'];
                    $model->address = $item['addr1'];
                    $model->address_op = $item['addr2'];
                    $model->city = $item['city'];
                    $model->state = $item['state'];
                    $model->zip = (string)$item['zip'];
                    $model->gender = $item['gender'] !=null ? $item['gender'] : null;
                    $model->yob = (string)$item['yob'];
                    $model->email = $item['email1'];
                    $model->email_op = $item['email2'];
                    $model->phone = $item['phone1'];
                    $model->phone_op = $item['phone2'];
                    $model->ice_contact = $item['ice'];
                    $model->ice_phone = $item['icephone'];
                    $model->club_name = $item['clubtxt'];
                    $model->club_id = $item['clubid'];
                    $model->mem_type = $this->findMemTypeId($item['memtype']);
                    $model->badge_type = $this->findMemTypeId($item['memtype']);
                    $model->primary = $item['primary']!=null ? $item['primary'] : null ;
                    $model->incep = $this->getNowTime();
                    $model->expires = date('Y-m-d',strtotime($item['expires']));
                    $model->qrcode = 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$item['barcode'];
                    $model->wt_date = $item['wtdate'];
                    $model->wt_instru = $item['wtinstru'];
                    $model->created_at = $this->getNowTime();
                    $model->updated_at = $this->getNowTime();
                    $model->payment_method = $item['payment_method'] !=null ? $item['payment_method'] : 'cash';
                    $model->remarks = 'created by bulk import';
                    $remarks = [
                        'created_at'=>$this->getNowTime(),
                        'data'=>$model->remarks,
                    ];
                    $tempArray [] = $remarks;
                    $model->remarks = json_encode($tempArray,true);
                    $payment_method = $model->payment_method;
                    $model->status ='active';
                    $feesReturns = $this->getFeesByType($model->mem_type);
                    if($item['badge_fee']!='0' && $item['discount']!=null && $item['amt_due']!='0') {
                        $model->badge_fee = $item['badge_fee'];
                        $model->amt_due = $item['amt_due'];
                        $model->discounts = $item['discount'];
                    }
                    else {
                        $model->badge_fee = $feesReturns['badgeFee'];
                        $model->amt_due = $feesReturns['badgeFee'];
                        $model->discounts = 0.00;
                    }
                    

                
                    if($model->save(false)) {

                        $badgeSubscriptionsModel =  new BadgeSubscriptions();
                        $badgeSubscriptionsModel->badge_number = $model->badge_number;
                        $badgeSubscriptionsModel->valid_from = date('Y-m-d',strtotime($model->incep));
                        $badgeSubscriptionsModel->valid_true = date('Y-m-d',strtotime($model->expires));
                        $badgeSubscriptionsModel->payment_type = $payment_method;
                        $badgeSubscriptionsModel->status = $model->status;
                        $badgeSubscriptionsModel->created_at = $this->getNowTime();
                        $badgeSubscriptionsModel->badge_fee = $model->badge_fee;
                        $badgeSubscriptionsModel->paid_amount = $model->amt_due;
                        $badgeSubscriptionsModel->discount = $model->discounts;
                        $badgeSubscriptionsModel->transaction_type ='NEW';
                        $badgeSubscriptionsModel->club_id = $model->club_id;
                        if($badgeSubscriptionsModel->save(false)) {
                            $model->badge_subscription_id = $badgeSubscriptionsModel->id;
                            if($model->save(false)) {     
                            }
                        }
                    }

               }
               else {

                $errorArray [] = $item;
               }

            } //foreach
        
            $successCount = count($items) - count($errorArray);
            $responce = [
                'status'=>'success',
                'errorCount' => count($errorArray),
                'errorArray' => $errorArray,
                'successCount' => $successCount,
            ];
            
            return json_encode($responce,true);
        
        
        }
        else {

            return $this->render('import-form',[
            ]);    
        }
        
    }



    public function getFeesByType($id) {
        $feeArray =  FeesStructure::find()->where(['membership_id'=>$id])->one();
        $feeOffer = $this->getOfferFee($feeArray);
        return $feeOffer;
    }

    protected function getOfferFee($feeArray) {
        $now = $this->getNowTime();
        $nowMonthOnly = date('m',strtotime($now));
        if($nowMonthOnly>=7 && $nowMonthOnly<11) {
            $persontage = yii::$app->params['conf']['offer'];
            $fee = ($feeArray->fee / 100) * $persontage;
               
        }
        else {
            $fee = $feeArray->fee;
        }

        $discount = $feeArray->fee - $fee;
        $responce = [
            'badgeFee'=>$feeArray->fee,
            'badgeSpecialFee' =>$fee,
            'discount'=>$discount,
        ];

        return $responce;
    }



    protected function findMemTypeId($string) {

        $membershipType = MembershipType::find()
            ->where(['type'=>$string])
            ->one();
        return $membershipType->id !=null ? $membershipType->id : '0';
    }

    protected function getFees($id) {
        $url = 'http://associatedgunclub.local/fee-structure/fees-by-type?id='.$id;
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));
        $result = curl_exec($cURL);
        curl_close($cURL);
        return $result;
        
    }


    public function actionCreate()
    {
        $model = new Badges();

        if ($model->load(Yii::$app->request->post())) {

            //echo'<pre>'; print_r($model); die();
            $model->created_at = $this->getNowTime(); 
            $model->updated_at = $this->getNowTime(); 
            $model->expires = date('Y-m-d H:i:s',strtotime($model->expires));
            $model->wt_date = date('Y-m-d H:i:s',strtotime($model->wt_date));
            $model->incep = date('Y-m-d H:i:s',strtotime($model->incep));
            $clubArray = Clubs::findOne($model->club_id);
            $model->club_name = $clubArray->club_name;
            $payment_method = $model->payment_method;
            $remarks = [
                'created_at'=>$this->getNowTime(),
                'data'=>$model->remarks,
            ];

            $tempArray [] = $remarks;
            $model->remarks = json_encode($tempArray,true);
            if($model->save()) {
                
                $model->status ='active';
                $model->qrcode='https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$model->badge_number;
                //$model->save(false);
                if($model->save()) {

                    $badgeSubscriptionsModel =  new BadgeSubscriptions();
                    $badgeSubscriptionsModel->badge_number = $model->badge_number;
                    $badgeSubscriptionsModel->valid_from = date('Y-m-d',strtotime($model->incep));
                    $badgeSubscriptionsModel->valid_true = date('Y-m-d',strtotime($model->expires));
                    $badgeSubscriptionsModel->payment_type = $payment_method;
                    $badgeSubscriptionsModel->status = $model->status;
                    $badgeSubscriptionsModel->created_at = $this->getNowTime();
                    $badgeSubscriptionsModel->badge_fee = $model->badge_fee;
                    $badgeSubscriptionsModel->paid_amount = $model->amt_due;
                    $badgeSubscriptionsModel->discount = $model->discounts;
                    $badgeSubscriptionsModel->transaction_type ='NEW';
                    $badgeSubscriptionsModel->club_id = $model->club_id;
                    if($badgeSubscriptionsModel->save(false)) {
                        $model->badge_subscription_id = $badgeSubscriptionsModel->id;
                        if($model->save()) {     
                        }
                        else {
                            echo'<pre>'; print_r($model->getErrors()); die();
                        }
                    }
                    else {
                      echo'<pre>'; print_r($badgeSubscriptionsModel->getErrors()); die();  
                    }

                    Yii::$app->getSession()->setFlash('success', 'Badge Holder Details has been created');
                    return $this->redirect(['view', 'id' => $model->badge_number]);

                }

                
            }
            else {

                $errors = $model->getErrors();
                if(array_key_exists('sticker', $errors)) {

                    $remarkDecode = json_decode($model->remarks,true);
                    $model->remarks = $remarkDecode[0]['data']; 
                    Yii::$app->getSession()->setFlash('error', 'Sticker '.$model->sticker.' has already been taken.');
                    return $this->render('/badges/create',['model'=>$model]);
                }
            }
            //echo'<pre>'; print_r($model); die();
           
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Badges model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $badgeSubscriptions = new BadgeSubscriptions();
        $badgeCertification = new BadgeCertification();

        if ($model->load(Yii::$app->request->post())) {
           $remarksOld = json_decode($model->remarks,true);
           $model->updated_at = $this->getNowTime(); 
            $nowRemakrs = [
                'created_at'=>$this->getNowTime(),
                'data'=> $model->remarks_temp,
            ];
            array_push($remarksOld,$nowRemakrs);
            $model->remarks = json_encode($remarksOld,true);
            $clubArray = Clubs::findOne($model->club_id);
            $model->club_name = $clubArray->club_name;
            $model->wt_date = date('Y-m-d',strtotime($model->wt_date));
            $model->expires = date('Y-m-d',strtotime($model->expires));
            $model->incep = date('Y-m-d H:i:s',strtotime($model->incep));
            if($model->save(false)) {
                Yii::$app->getSession()->setFlash('success', 'Badge Holder Details has been updated');
                return $this->redirect(['view', 'id' => $model->badge_number]);
            }
            
        } else {

           return $this->render('update', [
                'model' => $model,
                'badgeSubscriptions'=>$badgeSubscriptions,
                'badgeCertification'=>$badgeCertification,
            ]);
        }
    }

    public function actionAddCertification($membership_id) {

        $model = new BadgeCertification();

        if ($model->load(Yii::$app->request->post())) {
            $model->badge_number = $membership_id;
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();
            
            $feeStructureModel = FeesStructure::findOne($model->certification_type);
            
            $model->fee = $feeStructureModel->fee;
            $model->discount= 0.00;
            $model->amount_due = $model->fee - $model->discount;
           
            //if(!$model->validate())
            if($model->save()) {
                
                Yii::$app->getSession()->setFlash('success', 'Certificate has been generated');
                return $this->redirect(['/badges/view-certificate', 'membership_id' => $membership_id, 'view_id'=>$model->id]);
            }
            else {
                
                $errors = $model->getErrors();
                if(array_key_exists('sticker', $errors)) {
                    Yii::$app->getSession()->setFlash('error', 'Sticker '.$model->sticker.' has already been taken.');
                    return $this->redirect(['/badges/update', 'id' => $membership_id]);
                }
                //echo'<pre>'; print_r($errors); die();
            }
            
            



         }  

    }


    public function actionGenerateNewSticker() {
        $responce  = [
            'sticker'=>$this->getStikker(),
        ];

        echo json_encode($responce,true);

    }


    public function getStikker() {

        $badgeCertification = new BadgeCertification();
        $presmission = true;
        while($presmission==true) {
            $sticker = $badgeCertification->generateSticker();
            $validate = $badgeCertification->validateSticker($sticker);
            if($validate==true) {
                return $sticker;
            }        
        }
    }
    
     /**
     * Deletes an existing Badges model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionGetBadgeDetails($badge_number) {
        $badgeArray = Badges::find()->where(['badge_number'=>$badge_number])->one();
        
        $responce = Json::encode($badgeArray,JSON_PRETTY_PRINT);
        echo $responce;
    }


    public function actionApiGenerateRenavalFee() {
        
        if(Yii::$app->request->post()) {
            $badgeNumber = $_POST['badgeNumber'];
            $badgeFee = $_POST['BadgeFee'];
            $credit = $_POST['credit'];
            $workcreditper = $badgeFee / 40;

            if($credit>40) {
                $discount = $workcreditper * 40;
                $redeemableCredit = 40;

            }
            else {
                $discount = $workcreditper * $credit;
                $redeemableCredit = $credit;
            }

           $responce = [
                
                'badgeNumber'=>$badgeNumber,
                'BadgeFee' => $badgeFee,
                'discount' => $discount,
                'amountDue' => $badgeFee - $discount,       
                'redeemableCredit'=> $redeemableCredit,

           ];

            echo json_encode($responce,true);
        }
    }



    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Badges model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Badges the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Badges::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function expireBadgeSubcription($subcription_id) {
        $subcriptionRecord = BadgeSubscriptions::findOne($subcription_id);
        $subcriptionRecord->status = 'expired';
        return $subcriptionRecord->save(false) ? 'true' : 'false';
        
    }
}
