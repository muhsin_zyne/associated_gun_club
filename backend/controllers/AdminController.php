<?php
namespace backend\Controllers;

use yii;
use common\models\User;

class AdminController extends \yii\web\Controller

{	

	public $activeUser;
    
    public $rootAdminPermission = [
        'Work Credits'=>['work-credits/index','work-credits/import','work-credits/sticky-form','work-credits/create','work-credits/update','work-credits/credit-transfer','work-credits/transfer-confirm','work-credits/transfer-view','work-credits/view','work-credits/delete','work-credits/transfer-form'],
        'Accounts' => ['accounts/index','accounts/create','accounts/update','accounts/view','accounts/delete'],
        'Badge Payments'=> ['badge-payments/index'],
        'Badges' =>['badges/index','badges/create','badges/import-badges','badges/generate-new-sticker','badges/add-certification','badges/api-generate-renaval-fee','badges/update','badges/view','badges/view-work-credits','badges/view-work-credits-log','badges/delete','badges/print-view','badges/get-family-badges','badges/renew-membership','badges/get-badge-details','badges/test','badges/post-print-transactions'],
        'Fees Structure'=>['fee-structure/ajaxmoney-convert','fee-structure/index','fee-structure/create','fee-structure/update','fee-structure/delete','fee-structure/view','fee-structure/fees-by-type','badges/view-certificate','badges/view-certifications-list','badges/update-certificate','badges/delete-certificate'],
        'Index' => ['site/index', 'site/error', 'site/logout','site/login','site/new-badge'],
        'Clubs' => ['clubs/index','clubs/create','clubs/update','clubs/view','clubs/delete','clubs/badge-rosters','clubs/import-form'],
        
        'Badge' => ['badge/badge-print-view','badges/view-subscriptions','badges/view-renewal-history','badge/index','badge/users-index','badge/edit-user','badge/view-user','badge/create-user','badge/admin-function','badge/work-credit-entry','badge/brows-work-credits','badge/work-credit-menu','badge/club-name-look-up','badge/club-name-create','badge/club-name-edit','badge/work-credit-transfer','badge/create', 'badge/update', 'site/logout','site/login','site/new-badge'],

    ];
    
    public $adminPermission = [
        'Work Credits'=>['work-credits/index','work-credits/create','work-credits/update','work-credits/credit-transfer','work-credits/transfer-confirm','work-credits/transfer-view','work-credits/view','work-credits/delete','work-credits/transfer-form'],
        'Accounts' => ['accounts/index','accounts/create','accounts/update','accounts/view','accounts/delete'],
        'Badge Payments'=> ['badge-payments/index'],
        'Badges' =>['badges/index','badges/create','badges/generate-new-sticker','badges/add-certification','badges/api-generate-renaval-fee','badges/update','badges/view','badges/view-work-credits','badges/view-work-credits-log','badges/delete','badges/print-view','badges/get-family-badges','badges/renew-membership','badges/get-badge-details','badges/test','badges/post-print-transactions'],
        'Fees Structure'=>['fee-structure/ajaxmoney-convert','fee-structure/index','fee-structure/create','fee-structure/update','fee-structure/delete','fee-structure/view','fee-structure/fees-by-type','badges/view-certificate','badges/view-certifications-list','badges/update-certificate','badges/delete-certificate'],
        'Index' => ['site/index', 'site/error', 'site/logout','site/login','site/new-badge'],
        'Clubs' => ['clubs/index','clubs/create','clubs/update','clubs/view','clubs/delete','clubs/badge-rosters','clubs/import-form'],
        
        'Badge' => ['badge/badge-print-view','badges/view-subscriptions','badges/view-renewal-history','badge/index','badge/users-index','badge/edit-user','badge/view-user','badge/create-user','badge/admin-function','badge/work-credit-entry','badge/brows-work-credits','badge/work-credit-menu','badge/club-name-look-up','badge/club-name-create','badge/club-name-edit','badge/work-credit-transfer','badge/create', 'badge/update', 'site/logout','site/login','site/new-badge'],

    ];
    
    public $userPermission = [
        'Index' => ['site/index', 'site/error', 'site/logout','site/login'],
    ];

    
    public function beforeAction($event) {
        
        if(!yii::$app->user->isGuest) {
            if(!$this->hasPermission(Yii::$app->controller->id."/".Yii::$app->controller->action->id)){
                throw new \yii\web\UnauthorizedHttpException();
            }
        }
        else if (yii::$app->user->isGuest) {   
            if(Yii::$app->controller->id."/".Yii::$app->controller->action->id=='site/login') {

            }
            else {

                return $this->redirect(['/site/login']);
            }
        }
        //$this->mainMenu= require('../config/Menu.php');
        return parent::beforeAction($event);
    }
        
    public function hasPermission($event) {
        
        if(isset(Yii::$app->user->id)) {
            $activeUser = $this->getActiveUser();
            
            if($activeUser->privilege=='1') {
                foreach ($this->rootAdminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
            else if($activeUser->privilege=='2') {
                foreach ($this->adminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }

            else if($activeUser->privilege=='3') {
                foreach ($this->userPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
                
        }
    } //.has permission ending

    public function getNowTime() {
        date_default_timezone_set(yii::$app->params['timeZone']);
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }

    protected function getActiveUser() {
        if(isset(yii::$app->user->id)) {
            $activeUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            return $activeUser;
        }
        
    }

    public function getNowDigit() {
        date_default_timezone_set(yii::$app->params['timeZone']);
        $dateTime = date('ymdHis');
        return $dateTime;
    }


    public function getCurrentUrl() {
        $controllerId = yii::$app->controller->id;
        $actionId = yii::$app->controller->action->id;
        $requestUrl = $_SERVER['REQUEST_URI'];
        $responce = [
            'controllerId'=>$controllerId,
            'actionId' => $actionId,
            'requestUrl' => $requestUrl,
        ];

        return $responce;
    }



} // main class ending