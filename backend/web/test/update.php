<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BadgeSubscriptions */

$this->title = 'Update Badge Subscriptions: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Badge Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="badge-subscriptions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
