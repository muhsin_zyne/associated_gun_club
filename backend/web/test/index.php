<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BadgeCertificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Badge Certifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="badge-certification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Badge Certification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'badge_number',
            'created_at',
            'updated_at',
            'sticker',
            // 'certification_type',
            // 'status',
            // 'fee',
            // 'discount',
            // 'amount_due',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
