<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BadgeSubscriptions */

$this->title = 'Create Badge Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Badge Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="badge-subscriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
