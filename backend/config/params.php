<?php
return [
    'adminEmail' => 'admin@example.com',
    'timeZone'=>'America/New_York',
    'rootUrl'=>'http://associatedgunclub.local',
    'stickerPre'=>'ST-',
    'conf'=> [
        'offer'=>'50',
    ],
    'maskMoneyOptions' => [
        'prefix' => 'US $',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2, 
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
