<?php
namespace backend\components;

use yii\base\Widget;
use yii\helpers\Html;

class Menu extends Widget{
	public $type;
	public $is_disable;
	public $mainMenu = [
		[
			'label'=>'Issue New Range Badge',
			'url' => '/badges/create',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],
		[
			'label'=>'Renew / Update Existing Badge',
			'url' => '/badges/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Guest Registration',
			'url' => '',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>1,
			'items'=> [],
		], 

		[
			'label'=>'Trap Sales',
			'url' => '',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>1,
			'items'=> [],
		],

		[
			'label'=>'Range Rule Citations',
			'url' => '',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>1,
			'items'=> [],
		],

		[
			'label'=>'Range Badge Credits',
			'url' => '/work-credits/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],	
		
		[
			'label'=>'Admin Functions',
			'url' => '/badge/admin-function',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Exit',
			'url' => '',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],	


		
	];


	public $adminMenu = [
		[
			'label'=>'Post / Print Transactions',
			'url' => '/badges/post-print-transactions',
			'target' =>'_self',
			'icon' =>'',
			'status' =>0,
			'items'=> [],
		],
		[
			'label'=>'Create Badge Rosters for Club',
			'url' => '/clubs/badge-rosters',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Browse  / Modify Fee Schedules',
			'url' => '/fee-structure/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		], 

		[
			'label'=>'View / Print Badge Holder Profile',
			'url' => '/badges/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Add / Remove Authorized Users',
			'url' => '/accounts/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Browse  Range Badge Database',
			'url' => '/badges/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],	
		
		[
			'label'=>'Browse / Modify Member Club List',
			'url' => '/clubs/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		[
			'label'=>'Browse  Work Credits Database',
			'url' => '/work-credits/index',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],	

		[
			'label'=>'Transfer / Void Work Credits',
			'url' => '/work-credits/transfer-form',
			'target' =>'_self',
			'icon' =>'user',
			'status' =>0,
			'items'=> [],
		],

		
	];


	
	public function init(){
		parent::init();
		/*if($this->type===null){
			$this->message= 'Welcome User';
		}else{
			$this->message= 'Welcome '.$this->message;
		}*/
	}
	
	public function run(){
		$menuGenerate = $this->generateMenu($this->type);
		echo $menuGenerate;
	}

	protected function generateMenu($type) {
		if($type=='home') {
			$html ='';
			foreach ($this->mainMenu as $menu) {
				$this->is_disable = false;
				if($menu['status']==0) {
					$this->is_disable = true;
				}
				
				//$html.= '<a href ="'.$menu['url'].'" target="'.$menu['target'].'"> <div class="menu-box"> <span> '.$menu['label'].' </span> </div> </a>';


				$html.= Html::a('<div class="menu-box"> <span> '.$menu['label'].' </span> </div>', [$menu['url']], ['target'=>$menu['target'] ,'class' => $this->is_disable ? '' : 'disabled' ]);

				//$html.= Html::a($menu['label'], [$menu['url']], ['target'=>$menu['target'] ,'class' => $this->is_disable ? 'btn btn-primary btn-block button-nav disabled' : 'btn btn-primary btn-block button-nav' ]);
				
			}
			return $html;
		}
		else if ($type=='admin') {

			$html ='';
			foreach ($this->adminMenu as $menu) {
				$this->is_disable = false;
				if($menu['status']==0) {
					$this->is_disable = true;
				}


				$html.= Html::a('<div class="menu-box"> <span> '.$menu['label'].' </span> </div>', [$menu['url']], ['target'=>$menu['target'] ,'class' => $this->is_disable ? '' : 'disabled' ]);
				
				//$html.= '<a href ="'.$menu['url'].'" target="'.$menu['target'].'"> <div class="menu-box"> <span> '.$menu['label'].' </span> </div> </a>';

				//$html.= Html::a($menu['label'], [$menu['url']], ['target'=>$menu['target'] ,'class' => $this->is_disable ? 'btn btn-primary btn-block button-nav disabled' : 'btn btn-primary btn-block button-nav' ]);
				
			}
			return $html;

		}

		

	}
}

?>