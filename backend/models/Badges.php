<?php

namespace backend\models;

use Yii;
use backend\models\Clubs;
use backend\models\MembershipType;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "badges".
 *
 * @property integer $badge_number
 * @property string $prefix
 * @property string $first_name
 * @property string $last_name
 * @property string $suffix
 * @property string $address
 * @property string $address_op
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $gender
 * @property string $yob
 * @property string $email
 * @property string $email_op
 * @property string $phone
 * @property string $phone_op
 * @property string $ice_contact
 * @property string $ice_phone
 * @property string $club_name
 * @property integer $club_id
 * @property integer $mem_type
 * @property string $badge_type
 * @property integer $primary
 * @property string $incep
 * @property string $expires
 * @property string $qrcode
 * @property string $wt_date
 * @property string $wt_instru
 * @property string $remarks
 * @property double $badge_fee
 * @property double $discounts
 * @property double $amt_due
 * @property string $payment_method
 * @property string $sticker
 */
class Badges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $cat;
    public $subcat;
    public $badge_fee;
    public $discounts;
    public $amt_due;
    public $payment_method;
    public $amount_paid;
    public $remarks_temp;


    public static function tableName()
    {
        return 'badges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'address', 'city', 'state', 'zip', 'gender', 'yob', 'phone', 'club_name', 'club_id', 'mem_type', 'badge_type', 'incep', 'expires','wt_date','discounts','amt_due','badge_fee','payment_method','sticker'], 'required'],
            [['address', 'gender', 'qrcode', 'remarks'], 'string'],
            [['incep', 'expires', 'wt_date','email_op','badge_subscription_id','work_credits','prefix','suffix','address_op','ice_phone','ice_contact','remarks','payment_method','remarks_temp','created_at','updated_at'], 'safe'],
            [['club_id', 'mem_type', 'primary'], 'integer'],
            [['badge_fee', 'discounts', 'amt_due'], 'number'],
            [['prefix', 'suffix'], 'string', 'max' => 15],
            [['yob'], 'number','message'=>'not a valid yob.'],
            [['yob'], 'string', 'max' => 4,'min'=>4],
            [['zip'],'number'],
            [['email','email_op'],'email'],
            [['first_name'], 'string', 'max' => 20],
            [['phone_op','phone','ice_phone'], 'match', 'pattern' => '/^[- 0-9() +]+$/', 'message' => 'Not a valid phone number.'],
            
            [['last_name', 'city', 'phone', 'phone_op', 'ice_phone'], 'string', 'max' => 25],
            [['zip'], 'string', 'max' => 10],
            [['email', 'email_op'], 'string', 'max' => 40],
            [['club_name'], 'string', 'max' => 52],
            [['wt_instru'], 'string', 'max' => 255],
            [['sticker'], 'unique',],
            [['sticker'], 'unique', 'targetClass' => 'backend\models\BadgeCertification', 'targetAttribute'=> ['sticker']],
            //[['sticker'], 'string', 'max' => 6],
            ['primary', 'required', 'when' => function ($model) {
                                                return $model->mem_type == '2';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#badges-mem_type').val() == '2'; }"
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'badge_number' => 'Badge Number',
            'prefix' => 'Prefix',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'suffix' => 'Suffix',
            'address' => 'Address',
            'address_op' => 'Address Optional',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'gender' => 'Gender',
            'yob' => 'YOB',
            'email' => 'Email',
            'email_op' => 'Email Optional',
            'phone' => 'Phone',
            'phone_op' => 'Phone Optional',
            'ice_contact' => 'Emergency Contact',
            'ice_phone' => 'Emergency Contact Phone',
            'club_name' => 'Club Name',
            'club_id' => 'Club ID',
            'mem_type' => 'Membership Type',
            'badge_type' => 'Badge ID',
            'primary' => 'Primary',
            'incep' => 'Incep',
            'expires' => 'Expires',
            'qrcode' => 'qrcode',
            'wt_date' => 'WT Date',
            'wt_instru' => 'WT Instructor',
            'remarks' => 'Remarks',
            'badge_fee' => 'Badge Fee',
            'discounts' => 'Discounts',
            'amt_due' => 'Amt Due',
            'payment_method' => 'Payment Method',
            'sticker' => 'Sticker',
            'membershipType.type'=>'Badge Type',
            'status'=>'Account Status',
            'amount_paid'=>'Paid Amount',

        ];
    }

    public function getMembershipType() {
        return $this->hasOne(MembershipType::className(),['id'=>'mem_type']);
    }

    public function getClubNamesList() {
        $clubs = Clubs::find()->where(['status'=>'0'])->all();
        return ArrayHelper::map($clubs,'id','club_name');
    }
    public function getMemberShipList() {
        $memberShip = MembershipType::find()->where(['status'=>'0'])->all();
        return ArrayHelper::map($memberShip,'id','type');
    }

}
