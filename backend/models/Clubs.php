<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clubs".
 *
 * @property integer $id
 * @property string $club_name
 * @property string $short_name
 * @property string $status
 */
class Clubs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['club_name', 'short_name', 'status','club_id'], 'required'],
            [['status'], 'string'],
            [['club_id'],'number'],
            [['club_id'],'string','max'=>10],
            ['club_id', 'unique', 'targetAttribute' => ['club_id'], 'message' => 'Already taken Club ID, please choose a unique.'],
            [['club_name'], 'string', 'max' => 255],
            [['short_name'], 'string', 'max' => 20],
            [['file'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Club ID',
            'club_id' => 'Club ID',
            'club_name' => 'Club Name',
            'short_name' => 'Short Name',
            'status' => 'Status',
        ];
    }
    public function getClubList() {
        $clubArray = Clubs::find()
            ->where(['status'=>'0'])
            ->all();
        return ArrayHelper::map($clubArray,'club_id','club_name');

    }
}
