<?php

namespace backend\models;

use Yii;
use backend\models\Badges;
use backend\models\WorkCreditTransactions;

/**
 * This is the model class for table "credit_transfer_queue".
 *
 * @property integer $id
 * @property integer $from_badge_number
 * @property integer $to_badge_number
 * @property string $created_at
 * @property double $work_hours
 * @property string $status
 * @property string $note
 * @property integer $created_by
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $created_role
 */
class CreditTransferQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $from_badge_name;
    public $to_badge_name;
    public $total_credit_avl;

    public static function tableName()
    {
        return 'credit_transfer_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_badge_number', 'to_badge_number', 'created_at', 'work_hours', 'note', 'created_role','approved_by'], 'required'],
            [['from_badge_number', 'to_badge_number', 'created_by', 'created_role'], 'integer'],
            [['created_at', 'approved_at','total_credit_avl'], 'safe'],
            [['work_hours'], 'number'],
            ['to_badge_number', 'compare', 'compareAttribute'=>'from_badge_number', 'operator' => '!=', 'message'=>"credit beneficiary badge number should be different from donor." ],
            [['work_hours','total_credit_avl'],'number'],
            ['work_hours', 'compare', 'compareAttribute'=>'total_credit_avl', 'operator' => '<=', 'message'=>"transfer credit should be less than or equal to total available credit of doner.",'type' => 'number'],
            [['approved_by'],'string','max'=>100],
            [['status', 'note'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_badge_number' => 'From Badge Number',
            'from_badge_name'=> 'Badge Holder',
            'total_credit_avl' => 'Available Credit Limit',
            'to_badge_name' => 'Badge Holder',
            'to_badge_number' => 'To Badge Number',
            'created_at' => 'Created At',
            'work_hours' => 'Work Hours',
            'status' => 'Status',
            'note' => 'Remarks',
            'created_by' => 'Created By',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'created_role' => 'Created Role',
            'frombadgeDetails.first_name'=>'Doner First Name',
            'frombadgeDetails.last_name'=>'Doner Last Name',
            'frombadgeDetails.badge_number' => 'Doner Badge Number',
            'tobadgeDetails.first_name'=>'Receiver First Name',
            'tobadgeDetails.last_name'=>'Receiver Last Name',
            'tobadgeDetails.badge_number' => 'Receiver Badge Number',
            'adminUser.username'=>'Issued By',

        ];
    }

    public function getFrombadgeDetails() {
        return $this->hasOne(Badges::className(),['badge_number'=>'from_badge_number']);
    }
    public function getTobadgeDetails() {
        return $this->hasOne(Badges::className(),['badge_number'=>'to_badge_number']);
    }
    public function getAdminUser() {
        return $this->hasOne(User::className(),['id'=>'created_by']);
    }


    public function creditTransferRequest($from_badge_number,$credit) {
        $badgeModel = Badges::findOne($from_badge_number);

        if(!empty($badgeModel)) {
            $responce = [
                'fromBadgeNumber'=>$from_badge_number,
                'creditRequest'=>$credit,
                'avlCredits' => $badgeModel->work_credits,
                'status' => false,
            ];
            if($badgeModel->work_credits>=$credit) {
                $responce['status'] = true;

            }
            return $responce;
        }
        
    }
    public function doTransferCredit($from_badge_number,$to_badge_number,$credit){
        $fromBadgeModel = Badges::findOne($from_badge_number);
        $toBadgeModel = Badges::findOne($to_badge_number);
        $fromBadgeModel->work_credits = $fromBadgeModel->work_credits - $credit;
        
        $workCreditTransactions = new WorkCreditTransactions();
        $workCreditTransactions->badge_number = $fromBadgeModel->badge_number;
        $workCreditTransactions->type = 'credit';
        $workCreditTransactions->value = $credit;
        $workCreditTransactions->remarks = 'Gift transfer to #'.$toBadgeModel->badge_number;
        $workCreditTransactions->created_at = yii::$app->controller->getNowTime();
        $workCreditTransactions->updated_at = yii::$app->controller->getNowTime();
        $workCreditTransactions->save();

        $workCreditTransactions = new WorkCreditTransactions();
        $workCreditTransactions->badge_number = $toBadgeModel->badge_number;
        $workCreditTransactions->type='debit';
        $workCreditTransactions->value = $credit;
        $workCreditTransactions->remarks = 'Gift work credit from #'.$fromBadgeModel->badge_number;
        $workCreditTransactions->created_at = yii::$app->controller->getNowTime();
        $workCreditTransactions->updated_at = yii::$app->controller->getNowTime();
        $workCreditTransactions->save();
        
        if($fromBadgeModel->save(false)) {
            $toBadgeModel->work_credits = $toBadgeModel->work_credits + $credit;
            if($toBadgeModel->save(false)) {
                return true;
            }
        }
        else {
            return false;
        }
    }
}
