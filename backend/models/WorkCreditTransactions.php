<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "work_credit_transactions".
 *
 * @property integer $id
 * @property string $type
 * @property double $value
 * @property string $remarks
 * @property string $created_at
 * @property string $updated_at
 */
class WorkCreditTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_credit_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'value', 'remarks', 'created_at', 'updated_at','badge_number'], 'required'],
            [['type', 'remarks'], 'string'],
            [['value'], 'number'],
            [['created_at', 'updated_at','work_credits_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'value' => 'Value',
            'remarks' => 'Remarks',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'badge_number' => 'Badge Number',
        ];
    }
}
