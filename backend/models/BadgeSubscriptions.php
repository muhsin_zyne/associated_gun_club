<?php

namespace backend\models;

use Yii;
use backend\models\Badges;
use backend\models\Clubs;
/**
 * This is the model class for table "badge_payments".
 *
 * @property integer $id
 * @property integer $badge_number
 * @property string $valid_from
 * @property string $valid_true
 * @property string $payment_type
 * @property string $status
 * @property string $created_at
 */
class BadgeSubscriptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public $mem_id;
    public $mem_type;
    public $expires;
    public $amount_due;
    public $redeemable_credit;
    public $total_credit;
    public $sticker;


    public static function tableName()
    {
        return 'badge_subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['badge_number', 'valid_from', 'valid_true', 'payment_type', 'status', 'created_at','badge_fee'], 'required'],
            [['badge_number'], 'integer'],
            ['amount_due', 'required', 'when' => function ($model) {

                $currentUrl = yii::$app->controller->getCurrentUrl();
                if($currentUrl['controllerId']=='badges'&&$currentUrl['actionId']=='update') {
                    return true;
                }
              }, 
            ],
            [['valid_from', 'valid_true', 'created_at','badge_fee','paid_amount','discount','mem_id','mem_type','expires','amount_due','redeemable_credit','transaction_type','club_id'], 'safe'],
            [['sticker'],'required'],
            [['sticker'], 'unique', 'targetClass' => 'backend\models\Badges', 'targetAttribute'=> ['sticker']],
            [['sticker'], 'unique', 'targetClass' => 'backend\models\BadgeCertification', 'targetAttribute'=> ['sticker']],
            [['payment_type', 'status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'badge_number' => 'Badge Number',
            'valid_from' => 'Valid From',
            'valid_true' => 'Valid True',
            'payment_type' => 'Payment Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'mem_type'=>'Membership Type',
        ];
    }

    public function getBadgeDetails() {
        return $this->hasOne(Badges::className(),['badge_number'=>'badge_number']);
    }
    public function getClubDetails() {
        return $this->hasOne(Clubs::className(),['id'=>'club_id']);
    }


}
