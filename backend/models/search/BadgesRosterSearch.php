<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Badges;
use backend\models\Clubs;
use yii\helpers\ArrayHelper;

/**
 * BadgesSearch represents the model behind the search form about `backend\models\Badges`.
 */
class BadgesRosterSearch extends Badges
{
    /**
     * @inheritdoc
     */
    public $club_id_dummy;

    public function rules()
    {
        return [
            [['club_id','club_id_dummy'], 'integer'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Badges::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if($this->club_id_dummy=='') {
            $this->club_id_dummy=0;
        }
        $expiredBefore = date('Y-m-d',strtotime("-5 year", strtotime(yii::$app->controller->getNowTime())));
        $expireAfter = date('Y-m-d',strtotime("+30 year", strtotime(yii::$app->controller->getNowTime())));

        
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //echo'<pre>'; print_r($this->badge_type); die();

        $query->andFilterWhere([
            'club_id' => $this->club_id_dummy,
        ]);

    
        //$query->andFilterWhere(['between', 'expires', $expiredBefore, $expireAfter ]);
         //   ->andFilterWhere(['like', 'first_name', $this->first_name]);


        return $dataProvider;
    }


    public function getClubList() {
        $clubArray = Clubs::find()
            ->where(['status'=>'0'])
            ->all();
        return ArrayHelper::map($clubArray,'id','club_name');

    }
}
