<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\WorkCredits;

/**
 * WorkCreditsSearch represents the model behind the search form about `backend\models\WorkCredits`.
 */
class WorkCreditsSearch extends WorkCredits
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'badge_number'], 'integer'],
            [['work_date', 'project_name', 'remarks', 'authorized_by', 'status', 'updated_at'], 'safe'],
            [['work_hours'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkCredits::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'badge_number' => $this->badge_number,
            'work_date' => $this->work_date,
            'work_hours' => $this->work_hours,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'authorized_by', $this->authorized_by])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
