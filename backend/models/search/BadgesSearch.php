<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Badges;

/**
 * BadgesSearch represents the model behind the search form about `backend\models\Badges`.
 */
class BadgesSearch extends Badges
{
    /**
     * @inheritdoc
     */
    public $expire_date_range;
    public $expire_condition;
    public $nowDateplus2;
    public $nowDate;
    public $nowDateMin2;
    public $nowDateMin5;

    public function rules()
    {
        return [
            [['badge_number', 'club_id', 'mem_type', 'primary'], 'integer'],
            [['prefix', 'first_name', 'last_name', 'suffix', 'address', 'address_op', 'city', 'state', 'zip', 'gender', 'yob', 'email', 'email_op', 'phone', 'phone_op', 'ice_contact', 'ice_phone', 'club_name', 'badge_type', 'incep', 'expires', 'qrcode', 'wt_date', 'wt_instru', 'remarks', 'payment_method', 'sticker','status','expire_date_range','expire_condition'], 'safe'],
            [['badge_fee', 'discounts', 'amt_due'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Badges::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $this->nowDateplus2 = date('Y-m-d', strtotime("+2 years",strtotime(yii::$app->controller->getNowTime())));
        $this->nowDateMin2 = date('Y-m-d', strtotime("-2 years",strtotime(yii::$app->controller->getNowTime())));
        $this->nowDate = date('Y-m-d',strtotime(yii::$app->controller->getNowTime()));
        $this->nowDateMin5 = date('Y-m-d', strtotime("-5 years",strtotime(yii::$app->controller->getNowTime())));

        //print_r($this->nowDateplus2); die(); 

        if(!isset($this->expire_condition)) {
            $this->expire_condition = 'active+2';
        }




        if($this->expire_date_range==null) {
            $expireDateRange[0] = '2000-01-01';
            $expireDateRange[1] = '2100-01-01';
        }
        else  {
            $tempexpireDateRange = explode(' - ', $this->expire_date_range);
            $expireDateRange[0] = date('Y-m-d',strtotime($tempexpireDateRange[0]));
            $expireDateRange[1] = date('Y-m-d',strtotime($tempexpireDateRange[1]));
        }
        
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //echo'<pre>'; print_r($this->badge_type); die();

        if($this->expires!=null) {
            $expireDate = date('Y-m-d',strtotime($this->expires));
            //echo'<pre>'; print_r($this->expires); die();
        }
        else {
            $expireDate='';
        }
        //$this->expires = date('Y-m-d',strtotime($this->expires));
        //echo'<pre>'; print_r($this->expires); die();
        // grid filtering conditions
        $query->andFilterWhere([
            //'badge_number' => $this->badge_number,
            'status'=>$this->status,
            'yob' => $this->yob,
            'club_id' => $this->club_id,
            'mem_type' => $this->mem_type,
            'primary' => $this->primary,
            'incep' => $this->incep,
            'badge_type'=>$this->badge_type,
            //'expires' => '',
            'wt_date' => $this->wt_date,
            'badge_fee' => $this->badge_fee,
            'discounts' => $this->discounts,
            'amt_due' => $this->amt_due,
        ]);

        if($this->expire_condition=='active+2') {
            $query->andFilterWhere(['between','expires',$this->nowDate,$this->nowDateplus2]);
            //$query->andFilterWhere(['>=','expires',$this->nowDateplus2]);
        }
        else if($this->expire_condition=='active') {
            $query->andFilterWhere(['>=','expires',$this->nowDate]);
        }

        else if($this->expire_condition=='expired<2') {
            $query->andFilterWhere(['between','expires',$this->nowDateMin2,$this->nowDate]);
        }
        else if($this->expire_condition=='expired>2') {
             $query->andFilterWhere(['>','expires',$this->nowDateplus2]);
        }
        else if($this->expire_condition=='inactive') {
             $query->andFilterWhere(['<=','expires',$this->nowDateMin5]);
        }

        else if($this->expire_condition=='all') {
             $query->andFilterWhere(['between', 'expires', '1999-01-01', '2999-01-01']);
        }


        $query->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'badge_number', $this->badge_number])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'suffix', $this->suffix])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'address_op', $this->address_op])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'email_op', $this->email_op])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_op', $this->phone_op])
            ->andFilterWhere(['like', 'ice_contact', $this->ice_contact])
            ->andFilterWhere(['like', 'ice_phone', $this->ice_phone])
            ->andFilterWhere(['like', 'club_name', $this->club_name])
            //->andFilterWhere(['like', 'badge_type', $this->badge_type])
            ->andFilterWhere(['like', 'qrcode', $this->qrcode])
            ->andFilterWhere(['like', 'wt_instru', $this->wt_instru])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'expires', $expireDate])
            ->andFilterWhere(['like', 'payment_method', $this->payment_method])
            //->andFilterWhere(['status', 'sticker', $this->status]
            ->andFilterWhere(['like', 'sticker', $this->sticker]);
            
            $query->andFilterWhere(['between', 'expires', $expireDateRange[0], $expireDateRange[1] ]);


        return $dataProvider;
    }
}
