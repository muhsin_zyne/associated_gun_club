<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CreditTransferQueue;

/**
 * CreditTransferQueueSearch represents the model behind the search form about `backend\models\CreditTransferQueue`.
 */
class CreditTransferQueueSearch extends CreditTransferQueue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_badge_number', 'to_badge_number', 'created_by', 'approved_by', 'created_role'], 'integer'],
            [['created_at', 'status', 'note', 'approved_at'], 'safe'],
            [['work_hours'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CreditTransferQueue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'from_badge_number' => $this->from_badge_number,
            'to_badge_number' => $this->to_badge_number,
            'created_at' => $this->created_at,
            'work_hours' => $this->work_hours,
            'created_by' => $this->created_by,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'created_role' => $this->created_role,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
